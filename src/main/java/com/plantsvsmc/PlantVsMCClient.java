package com.plantsvsmc;

import com.plantsvsmc.block.ModBlocks;
import com.plantsvsmc.entity.ModEntities;
import com.plantsvsmc.entity.client.*;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.minecraft.client.render.RenderLayer;

public class PlantVsMCClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {

        ModEntities.registerModModelEnitity();
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.PEA_CROP, RenderLayer.getCutout());

    }
}
