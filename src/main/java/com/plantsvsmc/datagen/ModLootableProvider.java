package com.plantsvsmc.datagen;

import com.plantsvsmc.block.ModBlocks;
import com.plantsvsmc.block.custom.PeaCropBlock;
import com.plantsvsmc.item.ModItems;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricBlockLootTableProvider;
import net.minecraft.block.Blocks;
import net.minecraft.data.server.loottable.vanilla.VanillaBlockLootTableGenerator;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.Items;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.condition.BlockStatePropertyLootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.entry.LootPoolEntry;
import net.minecraft.loot.function.ApplyBonusLootFunction;
import net.minecraft.predicate.StatePredicate;

public class ModLootableProvider extends FabricBlockLootTableProvider {
    public ModLootableProvider(FabricDataOutput dataOutput) {
        super(dataOutput);
    }

    @Override
    public void generate() {
        BlockStatePropertyLootCondition.Builder builder = BlockStatePropertyLootCondition.builder(ModBlocks.PEA_CROP).properties(StatePredicate.Builder.create()
                .exactMatch(PeaCropBlock.AGE, 5));
        addDrop(ModBlocks.PEA_CROP, cropDrops(ModBlocks.PEA_CROP, ModItems.PEA, ModItems.PEA, builder));

    }
}
