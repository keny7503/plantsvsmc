package com.plantsvsmc.datagen;

import com.plantsvsmc.item.ModItems;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricRecipeProvider;
import net.minecraft.data.server.recipe.RecipeJsonProvider;
import net.minecraft.data.server.recipe.ShapedRecipeJsonBuilder;
import net.minecraft.data.server.recipe.ShapelessRecipeJsonBuilder;
import net.minecraft.item.Items;
import net.minecraft.recipe.book.RecipeCategory;
import net.minecraft.util.Identifier;

import java.util.function.Consumer;

public class ModRecipeProvider extends FabricRecipeProvider {
    public ModRecipeProvider(FabricDataOutput output) {
        super(output);
    }

    @Override
    public void generate(Consumer<RecipeJsonProvider> exporter) {
        ShapedRecipeJsonBuilder.create(RecipeCategory.MISC, ModItems.UNLIT_SUN,1)
                .pattern("WWW")
                .pattern("WWW")
                .pattern("WWW")
                .input('W', Items.SUNFLOWER)
                .criterion(hasItem(Items.SUNFLOWER),conditionsFromItem(Items.SUNFLOWER))
                .criterion(hasItem(ModItems.UNLIT_SUN),conditionsFromItem(ModItems.UNLIT_SUN))
                .offerTo(exporter, new Identifier(getRecipeName(ModItems.UNLIT_SUN)));
        ShapelessRecipeJsonBuilder.create(RecipeCategory.TOOLS,ModItems.SUNFLOWER_SPAWN_EGG,1)
                .input(ModItems.SUN)
                .input(Items.SUNFLOWER)
                .criterion(hasItem(Items.SUNFLOWER),conditionsFromItem(Items.SUNFLOWER))
                .offerTo(exporter, new Identifier(getRecipeName(ModItems.SUNFLOWER_SPAWN_EGG)));
    }
}
