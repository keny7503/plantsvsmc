package com.plantsvsmc.datagen;

import com.plantsvsmc.block.ModBlocks;
import com.plantsvsmc.block.custom.PeaCropBlock;
import com.plantsvsmc.item.ModItems;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricModelProvider;
import net.minecraft.data.client.BlockStateModelGenerator;
import net.minecraft.data.client.ItemModelGenerator;
import net.minecraft.data.client.Models;
import net.minecraft.item.ArmorItem;

public class ModModelProvider extends FabricModelProvider {
    public ModModelProvider(FabricDataOutput output) {
        super(output);
    }

    @Override
    public void generateBlockStateModels(BlockStateModelGenerator blockStateModelGenerator) {
        blockStateModelGenerator.registerCrop(ModBlocks.PEA_CROP, PeaCropBlock.AGE, 0, 1, 2, 3, 4, 5);
    }

    @Override
    public void generateItemModels(ItemModelGenerator itemModelGenerator) {
//        itemModelGenerator.register(ModItems.PEA, Models.GENERATED);
        itemModelGenerator.register(ModItems.PLANTESSENCE, Models.GENERATED);
        itemModelGenerator.register(ModItems.FILLEDPLANTESSENCE, Models.GENERATED);
        itemModelGenerator.register(ModItems.SUN, Models.GENERATED);
        itemModelGenerator.register(ModItems.UNLIT_SUN, Models.GENERATED);
        itemModelGenerator.register(ModItems.SUNFLOWER_SPAWN_EGG, Models.GENERATED);
        itemModelGenerator.register(ModItems.PEASHOOTER_SPAWN_EGG, Models.GENERATED);

        itemModelGenerator.register(ModItems.TRAFFIC_CONE, Models.GENERATED);
        itemModelGenerator.register(ModItems.IRON_BUCKET, Models.GENERATED);


    }
}
