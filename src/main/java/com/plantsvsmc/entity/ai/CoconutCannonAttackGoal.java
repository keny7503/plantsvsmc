package com.plantsvsmc.entity.ai;

import com.plantsvsmc.entity.custom.CoconutCannonEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.RangedAttackMob;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.MathHelper;
import org.jetbrains.annotations.Nullable;

import java.util.EnumSet;

public class CoconutCannonAttackGoal extends Goal {
    private final CoconutCannonEntity mob;
    private final RangedAttackMob owner;
    @Nullable
    private LivingEntity target;
    private int attackDelay;
    private int updateCountdownTicks;
    private final double mobSpeed;
    private int seenTargetTicks;
    private final int intervalTicks;
    private final float maxShootRange;
    private final float squaredMaxShootRange;



    public CoconutCannonAttackGoal(RangedAttackMob mob, double mobSpeed, int intervalTicks, int attackDelay, float maxShootRange) {
        this.updateCountdownTicks = -1;;
        if (!(mob instanceof LivingEntity)) {
            throw new IllegalArgumentException("ArrowAttackGoal requires Mob implements RangedAttackMob");
        } else {
            this.owner = mob;
            this.mob = (CoconutCannonEntity)mob;
            this.mobSpeed = mobSpeed;
            this.intervalTicks = intervalTicks;
            this.attackDelay = attackDelay;
            this.maxShootRange = maxShootRange;
            this.squaredMaxShootRange = maxShootRange * maxShootRange;
            this.setControls(EnumSet.of(Control.MOVE, Control.LOOK));
        }
    }

    public boolean canStart() {
        LivingEntity livingEntity = this.mob.getTarget();
        if (livingEntity != null && livingEntity.isAlive()) {
            this.target = livingEntity;
            return true;
        } else {
            return false;
        }
    }

    public boolean shouldContinue() {
        return this.canStart() || this.target.isAlive() && !this.mob.getNavigation().isIdle();
    }

    public void stop() {
        this.target = null;
        this.seenTargetTicks = 0;
        this.updateCountdownTicks = -1;
    }

    public boolean shouldRunEveryTick() {
        return true;
    }


    public void tick() {
        double d = this.mob.squaredDistanceTo(this.target.getX(), this.target.getY(), this.target.getZ());
        boolean bl = this.mob.getVisibilityCache().canSee(this.target);
        if (bl) {
            ++this.seenTargetTicks;
        } else {
            this.seenTargetTicks = 0;
        }

        if (!(d > (double)this.squaredMaxShootRange) && this.seenTargetTicks >= 5) {
            this.mob.getNavigation().stop();
        } else {
            this.mob.getNavigation().startMovingTo(this.target, this.mobSpeed);
        }

        this.mob.getLookControl().lookAt(this.target, 30.0F, 30.0F);
        if (--this.updateCountdownTicks == 0) {
            if (!bl) {
                return;
            }
            float f = (float)Math.sqrt(d) / this.maxShootRange;
            float g = MathHelper.clamp(f, 0.1F, 1.0F);
            this.owner.attack(this.target, g);
            this.updateCountdownTicks = intervalTicks;
        } else if (this.updateCountdownTicks < 0) {
            this.updateCountdownTicks = intervalTicks;
        }

    }
}
