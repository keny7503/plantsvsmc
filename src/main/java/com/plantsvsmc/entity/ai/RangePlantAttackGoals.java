package com.plantsvsmc.entity.ai;

import com.plantsvsmc.entity.custom.abtract.RangePlantEntity;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.RangedAttackMob;
import net.minecraft.entity.ai.goal.ProjectileAttackGoal;

public class RangePlantAttackGoals extends ProjectileAttackGoal{
    private final RangePlantEntity entity;
    private int attackDelay = 40;
    private int tickUntilNextAttack = 40;
    private boolean shouldCountTillNextAttack = false;
        public RangePlantAttackGoals(RangedAttackMob mob, double mobSpeed, int intervalTicks, float maxShootRange) {
        super(mob, mobSpeed, intervalTicks, intervalTicks, maxShootRange);
        entity = ((RangePlantEntity) mob);
    }

    @Override
    public void start(){
        super.start();
        attackDelay=40;
        tickUntilNextAttack=40;
    }

    protected void attack(LivingEntity enermy){
        if(isEnermyWithinAttackDistance(enermy)){
            shouldCountTillNextAttack=true;

            if(isTimeToStartAttackAnimation()){
                entity.setAttacking(true);
            }

            if(isTimeToAttack()){
                performAttack(enermy);
            } else {
                resetAttackCooldown();
                shouldCountTillNextAttack=false;
                entity.setAttacking(false);
                entity.attackAnimationCooldown=0;
            }
        }

    }

    private boolean isEnermyWithinAttackDistance(LivingEntity enermy){
        return this.entity.distanceTo(enermy) <= 10.0f;
    }

    protected void resetAttackCooldown(){
        this.tickUntilNextAttack=this.getTickCount(attackDelay*2);
    }

    protected boolean isTimeToAttack(){
        return this.tickUntilNextAttack<=0;
    }

    protected boolean isTimeToStartAttackAnimation(){
        return this.tickUntilNextAttack <= attackDelay;
    }

    protected int getTickUntilNextAttack(){
        return this.tickUntilNextAttack;
    }

    protected void performAttack(LivingEntity enermy){
        this.resetAttackCooldown();
        //this.mob.tryAttack(enermy);
    }

    @Override
    public void tick(){
        super.tick();
        if(shouldCountTillNextAttack){
            this.tickUntilNextAttack=Math.max(this.tickUntilNextAttack-1,0);
        }
    }

    @Override
    public void stop(){
        entity.setAttacking(false);
        super.stop();
    }
}
