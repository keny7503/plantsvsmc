package com.plantsvsmc.entity.ai;

import java.util.EnumSet;

import com.plantsvsmc.entity.custom.PZombieEntiy;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.pathing.Path;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.util.Hand;

public class PZombieAttackGoals extends Goal{
    private final PZombieEntiy zombie;
    //protected final PathAwareEntity mob;
    private final double speed;
    private final boolean pauseWhenMobIdle;
    private Path path;
    private double targetX;
    private double targetY;
    private double targetZ;
    private int updateCountdownTicks;
    private int cooldown;
    private final int attackIntervalTicks = 20;
    private long lastUpdateTime;
    private static final long MAX_ATTACK_TIME = 20L;
    private int ticks;

   //public PZombieAttackGoals(PZombieEntiy zombie, double speed, boolean pauseWhenMobIdle) {
//     super(zombie, speed, pauseWhenMobIdle);
//     this.zombie = zombie;
//  }
    public PZombieAttackGoals(PZombieEntiy zombie, double speed, boolean pauseWhenMobIdle) {
        this.zombie = zombie;
        this.speed = speed;
        this.pauseWhenMobIdle = pauseWhenMobIdle;
        this.setControls(EnumSet.of(Control.MOVE, Control.LOOK));
    }

    public boolean canStart() {
        long l = this.zombie.getWorld().getTime();
        if (l - this.lastUpdateTime < 20L) {
        return false;
        } else {
        this.lastUpdateTime = l;
        LivingEntity livingEntity = this.zombie.getTarget();
        if (livingEntity == null) {
            return false;
        } else if (!livingEntity.isAlive()) {
            return false;
        } else {
            this.path = this.zombie.getNavigation().findPathTo(livingEntity, 0);
            if (this.path != null) {
                return true;
            } else {
                return this.getSquaredMaxAttackDistance(livingEntity) >= this.zombie.squaredDistanceTo(livingEntity.getX(), livingEntity.getY(), livingEntity.getZ());
            }
        }
        }
    }

    public boolean shouldContinue() {
        LivingEntity livingEntity = this.zombie.getTarget();
        if (livingEntity == null) {
        return false;
        } else if (!livingEntity.isAlive()) {
        return false;
        } else if (!this.pauseWhenMobIdle) {
        return !this.zombie.getNavigation().isIdle();
        } else if (!this.zombie.isInWalkTargetRange(livingEntity.getBlockPos())) {
        return false;
        } else {
        return !(livingEntity instanceof PlayerEntity) || !livingEntity.isSpectator() && !((PlayerEntity)livingEntity).isCreative();
        }
    }

    public void start() {
        this.zombie.getNavigation().startMovingAlong(this.path, this.speed);
        this.zombie.setAttacking(true);
        this.updateCountdownTicks = 0;
        this.cooldown = 0;
        this.ticks = 0;
    }

    public void stop() {
        LivingEntity livingEntity = this.zombie.getTarget();
        if (!EntityPredicates.EXCEPT_CREATIVE_OR_SPECTATOR.test(livingEntity)) {
        this.zombie.setTarget((LivingEntity)null);
        }

        this.zombie.setAttacking(false);
        this.zombie.getNavigation().stop();
        this.zombie.setAttacking(false);
    }

    public boolean shouldRunEveryTick() {
        return true;
    }

    public void tick() {
        LivingEntity livingEntity = this.zombie.getTarget();
        if (livingEntity != null) {
        this.zombie.getLookControl().lookAt(livingEntity, 30.0F, 30.0F);
        double d = this.zombie.getSquaredDistanceToAttackPosOf(livingEntity);
        this.updateCountdownTicks = Math.max(this.updateCountdownTicks - 1, 0);
        if ((this.pauseWhenMobIdle || this.zombie.getVisibilityCache().canSee(livingEntity)) && this.updateCountdownTicks <= 0 && (this.targetX == 0.0 && this.targetY == 0.0 && this.targetZ == 0.0 || livingEntity.squaredDistanceTo(this.targetX, this.targetY, this.targetZ) >= 1.0 || this.zombie.getRandom().nextFloat() < 0.05F)) {
            this.targetX = livingEntity.getX();
            this.targetY = livingEntity.getY();
            this.targetZ = livingEntity.getZ();
            this.updateCountdownTicks = 4 + this.zombie.getRandom().nextInt(7);
            if (d > 1024.0) {
                this.updateCountdownTicks += 10;
            } else if (d > 256.0) {
                this.updateCountdownTicks += 5;
            }

            if (!this.zombie.getNavigation().startMovingTo(livingEntity, this.speed)) {
                this.updateCountdownTicks += 15;
            }

            this.updateCountdownTicks = this.getTickCount(this.updateCountdownTicks);
        }

        this.cooldown = Math.max(this.cooldown - 1, 0);
        this.attack(livingEntity, d);
        }
        ++this.ticks;
        if (this.ticks >= 5 && this.getCooldown() < this.getMaxCooldown() / 2) {
           this.zombie.setAttacking(true);
        } else {
           this.zombie.setAttacking(false);
        }
    }

    protected void attack(LivingEntity target, double squaredDistance) {
        double d = this.getSquaredMaxAttackDistance(target);
        if (squaredDistance <= d && this.cooldown <= 0) {
        this.resetCooldown();
        this.zombie.swingHand(Hand.MAIN_HAND);
        this.zombie.tryAttack(target);
        }

    }

    protected void resetCooldown() {
        this.cooldown = this.getTickCount(20);
    }

    protected boolean isCooledDown() {
        return this.cooldown <= 0;
    }

    protected int getCooldown() {
        return this.cooldown;
    }

    protected int getMaxCooldown() {
        return this.getTickCount(20);
    }

    protected double getSquaredMaxAttackDistance(LivingEntity entity) {
        return (double)(this.zombie.getWidth() * 2.0F * this.zombie.getWidth() * 2.0F + entity.getWidth());
    }
}