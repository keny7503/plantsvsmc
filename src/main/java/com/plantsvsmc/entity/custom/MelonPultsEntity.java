package com.plantsvsmc.entity.custom;

//import static com.plantsvsmc.entity.custom.abtract.AttackPlantEntity.ATTACKING;

import com.plantsvsmc.entity.custom.MelonPultsEntity;
import com.plantsvsmc.entity.custom.abtract.RangePlantEntity;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.world.World;


public class MelonPultsEntity extends RangePlantEntity {
    public MelonPultsEntity(EntityType<? extends RangePlantEntity> entityType, World world) {
        super(entityType, world);
    } 

    public static DefaultAttributeContainer.Builder createMelonPultsAttributes(){
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 20)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0f)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 4)
                .add(EntityAttributes.GENERIC_ATTACK_SPEED,20);
    }
}
