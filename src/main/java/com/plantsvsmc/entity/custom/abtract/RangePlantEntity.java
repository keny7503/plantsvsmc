package com.plantsvsmc.entity.custom.abtract;

import com.plantsvsmc.entity.ai.RangePlantAttackGoals;
import com.plantsvsmc.entity.custom.MelonPultsEntity;
import com.plantsvsmc.entity.custom.PZombieEntiy;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.RangedAttackMob;
import net.minecraft.entity.ai.goal.ActiveTargetGoal;
import net.minecraft.entity.ai.goal.ProjectileAttackGoal;
import net.minecraft.entity.mob.SkeletonEntity;
import net.minecraft.world.World;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.ProjectileUtil;
import net.minecraft.entity.projectile.SmallFireballEntity;
import net.minecraft.entity.projectile.thrown.SnowballEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundEvents;

public abstract class RangePlantEntity extends AttackPlantEntity implements RangedAttackMob{
        public RangePlantEntity(EntityType<? extends AttackPlantEntity> entityType, World world) {
        super(entityType, world);
    }

    @Override
    protected void initGoals(){
        super.initGoals();
        this.goalSelector.add(1,new RangePlantAttackGoals(this, 1.5f, 19, 10.0f));
        this.targetSelector.add(2, new ActiveTargetGoal(this, PZombieEntiy.class, true));
    }

    @Override
    public void attack(LivingEntity target, float pullProgress){
        // SnowballEntity snowballEntity = new SnowballEntity(this.getWorld(), this);
        // double d = target.getEyeY() - (double)1.1f;
        // double e = target.getX() - this.getX();
        // double f = d - snowballEntity.getY();
        // double g = target.getZ() - this.getZ();
        // double h = Math.sqrt(e * e + g * g) * (double)0.2f;

        // snowballEntity.setVelocity(e*5, (f + h), g*5, 16f, 0.0f);
        // SmallFireballEntity smallFireballEntity = new SmallFireballEntity(this.getWorld(), this, e, f+h, g);
        // smallFireballEntity.setPosition(smallFireballEntity.getX(), this.getBodyY(0.5) , smallFireballEntity.getZ());

        // this.getWorld().spawnEntity(smallFireballEntity);

        ItemStack itemStack = this.getProjectileType(this.getStackInHand(ProjectileUtil.getHandPossiblyHolding(this, Items.BOW)));
        PersistentProjectileEntity persistentProjectileEntity = this.createArrowProjectile(itemStack, pullProgress);
        double d = target.getX() - this.getX();
        double e = target.getBodyY(0.3333333333333333) - persistentProjectileEntity.getY();
        double f = target.getZ() - this.getZ();
        double g = Math.sqrt(d * d + f * f);
        persistentProjectileEntity.setVelocity(d, e + g * 0.20000000298023224, f, 1.6F, (float)(14 - this.getWorld().getDifficulty().getId() * 4));
        this.playSound(SoundEvents.ENTITY_SKELETON_SHOOT, 1.0F, 1.0F / (this.getRandom().nextFloat() * 0.4F + 0.8F));
        this.getWorld().spawnEntity(persistentProjectileEntity);
    }

    protected PersistentProjectileEntity createArrowProjectile(ItemStack arrow, float damageModifier) {
        return ProjectileUtil.createArrowProjectile(this, arrow, damageModifier);
     }
}
