package com.plantsvsmc.entity.custom.abtract;

import com.plantsvsmc.item.ModItems;
import net.minecraft.entity.AnimationState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.LookAroundGoal;
import net.minecraft.entity.ai.goal.LookAtEntityGoal;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.loot.LootTables;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.util.Identifier;

public abstract class ProducePlantEntity extends PlantEntity{
    // Item drop cooldown
    protected int IntervalTicks;
    private int productionCountDownTicks = -1;
    protected Item productItem;
    public ProducePlantEntity(EntityType<? extends PlantEntity> entityType, World world, Item productItem, int IntervalTicks) {
        super(entityType, world);
        this.productItem = productItem;
        this.IntervalTicks = IntervalTicks;
    }



    @Override
    public void tick() {
        super.tick();
        // Check if the entity is on the server side
        if (this.isAlive()) {
            // Every tick countDown decrease until 0
            if (--this.productionCountDownTicks == 0){
                this.dropItem(productItem,1);
            } else if (this.productionCountDownTicks < 0) { // Reset countDown
                this.productionCountDownTicks = this.IntervalTicks;
            }
        }
    }
    @Override
    protected void initGoals() {
        this.goalSelector.add(5, new LookAtEntityGoal(this, PlayerEntity.class,5f));
        this.goalSelector.add(6, new LookAtEntityGoal(this, ZombieEntity.class,10f));
        this.goalSelector.add(7, new LookAroundGoal(this));

    }

//     @Override
//     public Identifier getLootTableId() {
//       if (this.isSheared()) {
//          return this.getType().getLootTableId();
//       } else {
//          Identifier var10000;
//          switch (2.field_6872[this.getColor().ordinal()]) {
//             case 1:
//                var10000 = LootTables.WHITE_SHEEP_ENTITY;
//                break;
//             case 2:
//                var10000 = LootTables.ORANGE_SHEEP_ENTITY;
//                break;
//             case 3:
//                var10000 = LootTables.MAGENTA_SHEEP_ENTITY;
//                break;
//             case 4:
//                var10000 = LootTables.LIGHT_BLUE_SHEEP_ENTITY;
//                break;
//             case 5:
//                var10000 = LootTables.YELLOW_SHEEP_ENTITY;
//                break;
//             case 6:
//                var10000 = LootTables.LIME_SHEEP_ENTITY;
//                break;
//             case 7:
//                var10000 = LootTables.PINK_SHEEP_ENTITY;
//                break;
//             case 8:
//                var10000 = LootTables.GRAY_SHEEP_ENTITY;
//                break;
//             case 9:
//                var10000 = LootTables.LIGHT_GRAY_SHEEP_ENTITY;
//                break;
//             case 10:
//                var10000 = LootTables.CYAN_SHEEP_ENTITY;
//                break;
//             case 11:
//                var10000 = LootTables.PURPLE_SHEEP_ENTITY;
//                break;
//             case 12:
//                var10000 = LootTables.BLUE_SHEEP_ENTITY;
//                break;
//             case 13:
//                var10000 = LootTables.BROWN_SHEEP_ENTITY;
//                break;
//             case 14:
//                var10000 = LootTables.GREEN_SHEEP_ENTITY;
//                break;
//             case 15:
//                var10000 = LootTables.RED_SHEEP_ENTITY;
//                break;
//             case 16:
//                var10000 = LootTables.BLACK_SHEEP_ENTITY;
//                break;
//             default:
//                throw new IncompatibleClassChangeError();
//          }

//          return var10000;
//       }
//    }
}
