package com.plantsvsmc.entity.custom.abtract;

import org.jetbrains.annotations.Nullable;

import com.plantsvsmc.entity.custom.abtract.PlantEntity;

import net.minecraft.entity.AnimationState;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.world.World;;

public abstract class PlantEntity extends MobEntity {
    public final AnimationState idlingAnimationState = new AnimationState();
    private int idleAnimationCooldown = 0;

    public PlantEntity(EntityType<? extends MobEntity> entityType, World world) {
            super(entityType, world);
    }

    protected void updateAnimations(){
        if (this.idleAnimationCooldown <= 0) {
            this.idleAnimationCooldown = this.random.nextInt(40) + 80;
            this.idlingAnimationState.start(this.age);
        } else {
            --this.idleAnimationCooldown;
        }
    }


    @Override
    public void tick() {
        super.tick();
        if(this.getWorld().isClient()){
            updateAnimations();
        }
    }

    public static DefaultAttributeContainer.Builder createAttributes(int max_health){
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, max_health)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0f);
    }

    @Nullable
    @Override
    protected SoundEvent getHurtSound(DamageSource source) {
        return SoundEvents.BLOCK_GRASS_STEP;
    }

    @Override
    public boolean cannotDespawn() {
        return this.isAlive();
    }
}
