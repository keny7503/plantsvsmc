package com.plantsvsmc.entity.custom.abtract;

import net.minecraft.entity.EntityType;
import net.minecraft.world.World;

public abstract class WalkThoughPlantEntity extends PlantEntity{
        public WalkThoughPlantEntity(EntityType<? extends PlantEntity> entityType, World world) {
        super(entityType, world);
    }
}
