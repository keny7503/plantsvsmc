package com.plantsvsmc.entity.custom.abtract;

import net.minecraft.entity.EntityType;
import net.minecraft.world.World;

public abstract class InstantUsePlantEntity extends PlantEntity{
        public InstantUsePlantEntity(EntityType<? extends PlantEntity> entityType, World world) {
        super(entityType, world);
    }
}
