package com.plantsvsmc.entity.custom.abtract;

import org.jetbrains.annotations.Nullable;

import com.plantsvsmc.entity.ai.PlantAttackGoals;
import com.plantsvsmc.entity.ai.RangePlantAttackGoals;

import net.minecraft.entity.AnimationState;
import net.minecraft.entity.Attackable;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.ActiveTargetGoal;
import net.minecraft.entity.ai.goal.LookAroundGoal;
import net.minecraft.entity.ai.goal.LookAtEntityGoal;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.world.World;

public class AttackPlantEntity extends PlantEntity {
    public final AnimationState attackAnimationState = new AnimationState();
    public int attackAnimationCooldown = 0;
    public AttackPlantEntity(EntityType<? extends PlantEntity> entityType, World world) {
        super(entityType, world);
    }

    private static final TrackedData<Boolean> ATTACKING = DataTracker.registerData(AttackPlantEntity.class, TrackedDataHandlerRegistry.BOOLEAN);

    @Override
    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(ATTACKING,false);
    }

    @Override
    public boolean isAttacking() {
        return this.dataTracker.get(ATTACKING);
    }

    public void setAttacking(boolean attacking) {
        this.dataTracker.set(ATTACKING,attacking);
    }

    @Override
    protected void updateAnimations(){
        super.updateAnimations();
        if(this.isAttacking() & attackAnimationCooldown <= 0){
            attackAnimationCooldown=40;
            attackAnimationState.start(this.age);
        } else {
            --this.attackAnimationCooldown;
        }
        
        if(!isAttacking()) {
            attackAnimationState.stop();
        }
    }

    @Override
    protected void initGoals() {
        this.goalSelector.add(3, new LookAtEntityGoal(this, ZombieEntity.class, 10.0f));
        this.goalSelector.add(4, new LookAroundGoal(this));
        this.targetSelector.add(1, new ActiveTargetGoal<MobEntity>(this, MobEntity.class, 10, true, false, entity -> entity instanceof Monster));
    }

    // public void attack(LivingEntity target, float pullProgress){

    // this.playSound(SoundEvents.ENTITY_SNOW_GOLEM_SHOOT, 1.0f, 0.4f / (this.getRandom().nextFloat() * 0.4f + 0.8f));
    
    // }

    @Nullable
    @Override
    protected SoundEvent getHurtSound(DamageSource source) {
        return SoundEvents.BLOCK_GRASS_STEP;
    }
}
