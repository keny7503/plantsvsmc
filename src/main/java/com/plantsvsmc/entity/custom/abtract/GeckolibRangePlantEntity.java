package com.plantsvsmc.entity.custom.abtract;

import com.plantsvsmc.entity.custom.PeashooterEnitity;
import com.plantsvsmc.entity.custom.projectile.PeaEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.RangedAttackMob;
import net.minecraft.entity.ai.goal.ActiveTargetGoal;
import net.minecraft.entity.ai.goal.ProjectileAttackGoal;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.sound.SoundEvents;
import net.minecraft.world.World;
import software.bernie.geckolib.core.animation.AnimatableManager;
import software.bernie.geckolib.core.animation.Animation;
import software.bernie.geckolib.core.animation.AnimationController;
import software.bernie.geckolib.core.animation.RawAnimation;

public class GeckolibRangePlantEntity extends GeckolibPlantEnitity implements RangedAttackMob {
    private LivingEntity target;
    private int attackDelayTick = -1;
    protected int attackDelay = 0;

    public GeckolibRangePlantEntity(EntityType<? extends MobEntity> entityType, World world, int attackDelay) {
        super(entityType, world);
        this.attackDelay =attackDelay;
    }

    @Override
    public void attack(LivingEntity target, float pullProgress) {
        this.swingHand(this.preferredHand);
        this.target = target;
        this.attackDelayTick = attackDelay;
    }
    public void shootProjectile(LivingEntity target){
        PeaEntity snowballEntity = new PeaEntity(this.getWorld(), this, 4.0f);
        double d = target.getEyeY() - (double)1.1f;
        double e = target.getX() - this.getX();
        double f = d - snowballEntity.getY();
        double g = target.getZ() - this.getZ();
        double h = Math.sqrt(e * e + g * g) * (double)0.2f  ;
        snowballEntity.setVelocity(e, (f + h), g, 1f, 0.0f);
        this.playSound(SoundEvents.ENTITY_SNOW_GOLEM_SHOOT, 1.0f, 0.4f / (this.getRandom().nextFloat() * 0.4f + 0.8f));
        this.getWorld().spawnEntity(snowballEntity);
    }


    @Override
    public void tick() {
        super.tick();
        System.out.println("Target:"+this.attackDelayTick+"tick "+this.handSwingTicks);
        if(this.attackDelayTick == 0 && this.target != null){
            this.shootProjectile(this.target);
        }
        if(this.attackDelayTick>=0) {
            this.attackDelayTick--;
        }
    }
}
