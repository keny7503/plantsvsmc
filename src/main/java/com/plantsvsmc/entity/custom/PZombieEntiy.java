package com.plantsvsmc.entity.custom;

// import javax.annotation.Nullable;

import com.plantsvsmc.entity.ai.PZombieAttackGoals;

import net.minecraft.entity.AnimationState;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.ActiveTargetGoal;
import net.minecraft.entity.ai.goal.LookAroundGoal;
import net.minecraft.entity.ai.goal.LookAtEntityGoal;
import net.minecraft.entity.ai.goal.RevengeGoal;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.entity.passive.IronGolemEntity;
import net.minecraft.entity.passive.MerchantEntity;
import net.minecraft.entity.passive.TurtleEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.world.World;

public class PZombieEntiy extends MobEntity{
    public final AnimationState idlingAnimationState = new AnimationState();
    private int idleAnimationTimeOut= 0;

    public PZombieEntiy(EntityType<? extends MobEntity> entityType, World world) {
        super(entityType, world);
    }

    protected void updateAnimations(){
        if (this.idleAnimationTimeOut <= 0) {
            this.idleAnimationTimeOut = this.random.nextInt(40) + 80;
            this.idlingAnimationState.start(this.age);
        } else {
            --this.idleAnimationTimeOut;
        }
    }

    @Override
    public void tick() {
        super.tick();
        if(this.getWorld().isClient()){
            updateAnimations();
        }
    }

    @Override
    protected void initGoals() {
        super.initGoals();
        this.goalSelector.add(8, new LookAtEntityGoal(this, PlayerEntity.class, 8.0F));
        this.goalSelector.add(8, new LookAroundGoal(this));
        this.initCustomGoals();
    }

   protected void initCustomGoals() {
        this.goalSelector.add(2, new PZombieAttackGoals(this, 0.05, false));
    //    this.goalSelector.add(7, new WanderAroundFarGoal(this, 1.0));
        this.targetSelector.add(2, new ActiveTargetGoal(this, MelonPultsEntity.class, true));
        this.targetSelector.add(2, new ActiveTargetGoal(this, PlayerEntity.class, true));
        this.targetSelector.add(3, new ActiveTargetGoal(this, MerchantEntity.class, false));
        this.targetSelector.add(3, new ActiveTargetGoal(this, IronGolemEntity.class, true));
   }

    public static DefaultAttributeContainer.Builder createPZombieAttribute(){
        return MobEntity.createMobAttributes()
            .add(EntityAttributes.GENERIC_MAX_HEALTH, 25)
            .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 5.0F)
            .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 4)
            .add(EntityAttributes.GENERIC_ATTACK_SPEED,20);
    }

    //@Nu
    @Override
    protected SoundEvent getHurtSound(DamageSource source) { 
        return SoundEvents.ENTITY_ZOMBIE_HURT;
    }

    //@Nullable
    @Override
    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_ZOMBIE_DEATH;
    }
}
