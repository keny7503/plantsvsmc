package com.plantsvsmc.entity.custom;

import com.plantsvsmc.entity.custom.abtract.GeckolibPlantEnitity;
import com.plantsvsmc.entity.custom.abtract.GeckolibRangePlantEntity;
import com.plantsvsmc.entity.custom.projectile.PeaEntity;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.RangedAttackMob;
import net.minecraft.entity.ai.goal.ActiveTargetGoal;
import net.minecraft.entity.ai.goal.LookAroundGoal;
import net.minecraft.entity.ai.goal.ProjectileAttackGoal;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.sound.SoundEvents;
import net.minecraft.world.World;
import software.bernie.geckolib.animatable.GeoEntity;

import software.bernie.geckolib.core.animatable.GeoAnimatable;
import software.bernie.geckolib.core.animatable.instance.AnimatableInstanceCache;
import software.bernie.geckolib.core.animation.*;
import software.bernie.geckolib.core.object.PlayState;
import software.bernie.geckolib.util.GeckoLibUtil;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class PeashooterEnitity extends GeckolibRangePlantEntity {
    private enum ANIMATION{
        IDLE("animation.peashooter.idle", Animation.LoopType.LOOP),
        ATTACK("animation.peashooter.attack", Animation.LoopType.HOLD_ON_LAST_FRAME)
        ;
        private final RawAnimation animation;
        ANIMATION(String animationName, Animation.LoopType loopType) {
            this.animation =  RawAnimation.begin().then(animationName, loopType);
        }
        public RawAnimation getRawAnimation(){ return animation;}
    }

    public PeashooterEnitity(EntityType<? extends MobEntity> entityType, World world) {
        super(entityType, world,0);
    }
    @Override
    protected void initGoals() {
        super.initGoals();
        this.goalSelector.add(2, new ProjectileAttackGoal(this, 1.5f,  60, 10.0f));
        this.targetSelector.add(2, new ActiveTargetGoal<>(this, ZombieEntity.class, true));
    }
    @Override
    public void shootProjectile(LivingEntity target) {
        PeaEntity snowballEntity = new PeaEntity(this.getWorld(), this, 4.0f);
        double d = target.getEyeY() - (double)1.1f;
        double e = target.getX() - this.getX();
        double f = d - snowballEntity.getY();
        double g = target.getZ() - this.getZ();
        double h = Math.sqrt(e * e + g * g) * (double)0.2f  ;
        snowballEntity.setVelocity(e, (f + h), g, 1f, 0.0f);
        this.playSound(SoundEvents.ENTITY_SNOW_GOLEM_SHOOT, 1.0f, 0.4f / (this.getRandom().nextFloat() * 0.4f + 0.8f));
        this.getWorld().spawnEntity(snowballEntity);
    }

    @Override
    public void registerControllers(AnimatableManager.ControllerRegistrar controllerRegistrar) {
        controllerRegistrar.add(new AnimationController<>(this, "controller", 0, animationState -> {
            if(this.handSwinging && this.handSwingTicks<30){
                this.handSwingTicks++;
                return animationState.setAndContinue(PeashooterEnitity.ANIMATION.ATTACK.getRawAnimation());
            }else {
                this.handSwinging = false;
                return animationState.setAndContinue(PeashooterEnitity.ANIMATION.IDLE.getRawAnimation());
            }
        }));
    }
}
