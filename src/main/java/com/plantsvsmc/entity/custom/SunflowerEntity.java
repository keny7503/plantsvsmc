package com.plantsvsmc.entity.custom;

import com.plantsvsmc.entity.custom.abtract.PlantEntity;
import com.plantsvsmc.entity.custom.abtract.ProducePlantEntity;
import com.plantsvsmc.item.ModItems;
import net.minecraft.entity.AnimationState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.AttackGoal;
import net.minecraft.entity.ai.goal.LookAroundGoal;
import net.minecraft.entity.ai.goal.LookAtEntityGoal;
import net.minecraft.entity.ai.goal.ProjectileAttackGoal;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.passive.SnowGolemEntity;
import net.minecraft.item.Item;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public class SunflowerEntity extends ProducePlantEntity {

    public SunflowerEntity(EntityType<? extends PlantEntity> entityType, World world) {
        // Produce a Unlit Sun every 100 tick
        super(entityType, world, ModItems.UNLIT_SUN, 100);
    }
    public static DefaultAttributeContainer.Builder createAttributes(){
        return createAttributes(20);
    }

    @Nullable
    @Override
    protected SoundEvent getHurtSound(DamageSource source) {
        return SoundEvents.BLOCK_GRASS_STEP;
    }
}
