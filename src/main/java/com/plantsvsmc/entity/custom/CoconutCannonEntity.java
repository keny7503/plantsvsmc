package com.plantsvsmc.entity.custom;

import com.plantsvsmc.entity.ai.CoconutCannonAttackGoal;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.RangedAttackMob;
import net.minecraft.entity.ai.goal.ActiveTargetGoal;
import net.minecraft.entity.ai.goal.LookAroundGoal;
import net.minecraft.entity.ai.goal.LookAtEntityGoal;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.entity.projectile.thrown.SnowballEntity;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.injection.At;

public class CoconutCannonEntity extends MobEntity implements RangedAttackMob {
    private static final TrackedData<Boolean> ATTACKING =
            DataTracker.registerData(CoconutCannonEntity.class, TrackedDataHandlerRegistry.BOOLEAN);
    public final AnimationState idleAnimationState = new AnimationState();
    private int idleAnimationTimeout = 0;
    public final AnimationState attackAnimationState = new AnimationState();
    public int attackAnimationTimeout = 0;

    private int attackDelayCountdown = -1;

    private LivingEntity target = null;

    private boolean attack = false;


    public CoconutCannonEntity(EntityType<? extends MobEntity> entityType, World world) {
        super(entityType, world);
    }

    public static DefaultAttributeContainer.Builder createAttributes(int max_health){
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, max_health)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0f);
    }


    public static DefaultAttributeContainer.Builder createAttributes(){
        return createAttributes(20);
    }


    protected void updateAnimations(){


        if (this.idleAnimationTimeout <= 0 && !isAttacking()) {
            this.idleAnimationTimeout = this.random.nextInt(40) + 80;
            attackAnimationState.stop();
            this.idleAnimationState.start(this.age);
        } else {
            --this.idleAnimationTimeout;
        }

        if(isAttacking() && attackAnimationTimeout <= 0) {
            attackAnimationTimeout = 100;
            idleAnimationState.stop();
            attackAnimationState.start(this.age);
        } else {
            --this.attackAnimationTimeout;
        }
        if(attackAnimationTimeout==1){
            setAttacking(false);
            idleAnimationTimeout = 0;
            attackAnimationTimeout = 0;
            attackDelayCountdown = -1;
            attackAnimationState.stop();
            idleAnimationState.stop();
            this.clearGoalsAndTasks();
            this.initGoals();
        }


    }
    public void setAttacking(boolean attacking) {
        this.dataTracker.set(ATTACKING, attacking);
    }

    @Override
    public boolean isAttacking() {
        return this.dataTracker.get(ATTACKING);
    }

    @Override
    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(ATTACKING, false);
    }
    @Override
    public void tick() {
        super.tick();

        if(this.getWorld().isClient()){
            System.out.println("Timeout "+attackAnimationTimeout+"Tick "+ isAttacking());

            updateAnimations();
        }
        if(--attackDelayCountdown == 0){
            if(target != null){
                shootProjectile(target);
            }
        }

    }

    @Override
    protected void initGoals() {
        this.goalSelector.add(1, new CoconutCannonAttackGoal(this, 1.5f, 120,7,50.0f));
        this.goalSelector.add(3, new LookAtEntityGoal(this, PlayerEntity.class, 6.0f));
        this.goalSelector.add(4, new LookAroundGoal(this));
        this.goalSelector.add(0, new LookAtEntityGoal(this, ZombieEntity.class, 6.0f));
        this.targetSelector.add(0, new ActiveTargetGoal<MobEntity>(this, MobEntity.class, 10, true, false, entity -> entity instanceof Monster));
    }


    @Override
    protected float getActiveEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return 0.7f;
    }
    @Override
    public void attack(LivingEntity target, float pullProgress) {
        this.target = target;
        attackDelayCountdown = 7;
        setAttacking(true);
        System.out.println("Setup attack "+isAttacking());
    }

    private void shootProjectile(LivingEntity target){
        System.out.println("Shoot "+isAttacking());
        SnowballEntity snowballEntity = new SnowballEntity(this.getWorld(), this);
        double d = target.getEyeY() - (double)1.1f;
        double e = target.getX() - this.getX();
        double f = d - snowballEntity.getY();
        double g = target.getZ() - this.getZ();
        double h = Math.sqrt(e * e + g * g) * (double)0.1f  ;
        snowballEntity.setVelocity(e, (f + h), g, 4f, 0.0f);
        CoconutBombEntity coconutBombEntity = new CoconutBombEntity(this.getWorld(),this,e,(f + h),g, 10f,7);
        this.playSound(SoundEvents.ENTITY_SNOW_GOLEM_SHOOT, 1.0f, 0.4f / (this.getRandom().nextFloat() * 0.4f + 0.8f));
        this.getWorld().spawnEntity(coconutBombEntity);
    }

    @Nullable
    @Override
    protected SoundEvent getHurtSound(DamageSource source) {
        return SoundEvents.BLOCK_GRASS_STEP;
    }


}
