package com.plantsvsmc.entity.client;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.entity.custom.MelonPultsEntity;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.util.Identifier;

public class MelonPultsRenderer extends MobEntityRenderer<MelonPultsEntity,MelonPultsModel<MelonPultsEntity>> {
    private static final Identifier TEXTURE = new Identifier(PlantsVsMC.MOD_ID,"textures/entity/melonpults.png");
    public MelonPultsRenderer(EntityRendererFactory.Context context) {
        super(context, new MelonPultsModel<>(context.getPart(ModModelLayers.MELONPULTS)), 0.5f);
    }

    @Override
    public Identifier getTexture(MelonPultsEntity entity) {
        return TEXTURE;
    }
}