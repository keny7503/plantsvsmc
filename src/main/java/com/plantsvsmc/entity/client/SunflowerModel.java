package com.plantsvsmc.entity.client;

import com.plantsvsmc.entity.animation.ModAnimations;
import com.plantsvsmc.entity.animation.SunflowerAnimations;
import com.plantsvsmc.entity.custom.SunflowerEntity;
import net.minecraft.client.model.*;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.render.entity.model.SinglePartEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;

// Made with Blockbench 4.9.2
// Exported for Minecraft version 1.17+ for Yarn
// Paste this class into your mod and generate all required imports
public class SunflowerModel<T extends SunflowerEntity> extends SinglePartEntityModel<T> {
	private final ModelPart sunflower;
	private final ModelPart head;
	private final ModelPart base;
	private final ModelPart stem;

	public SunflowerModel(ModelPart root) {

		this.sunflower = root.getChild("sunflower");
		this.head = sunflower.getChild("head");
		this.stem = sunflower.getChild("stem");
		this.base = sunflower.getChild("base");

	}
	public static TexturedModelData getTexturedModelData() {
		ModelData modelData = new ModelData();
		ModelPartData modelPartData = modelData.getRoot();
		ModelPartData sunflower = modelPartData.addChild("sunflower", ModelPartBuilder.create(), ModelTransform.of(0.0F, 24.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		ModelPartData head = sunflower.addChild("head", ModelPartBuilder.create().uv(12, 16).cuboid(-4.0F, -14.0F, 0.0F, 8.0F, 5.0F, 2.0F, new Dilation(0.0F))
		.uv(8, 9).cuboid(-5.0F, -14.0F, -1.0F, 10.0F, 5.0F, 2.0F, new Dilation(0.0F))
		.uv(12, 0).cuboid(-4.0F, -15.0F, -1.0F, 8.0F, 7.0F, 2.0F, new Dilation(0.0F)), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData bone = head.addChild("bone", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -12.0F, 0.0F));

		ModelPartData pedal = bone.addChild("pedal", ModelPartBuilder.create().uv(0, 20).cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)), ModelTransform.pivot(0.0F, -3.0F, 1.0F));

		ModelPartData cube_r1 = pedal.addChild("cube_r1", ModelPartBuilder.create().uv(0, 21).cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal2 = bone.addChild("pedal2", ModelPartBuilder.create().uv(0, 20).cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(2.0F, -3.0F, 1.0F, 0.0F, 0.0F, 0.3491F));

		ModelPartData cube_r2 = pedal2.addChild("cube_r2", ModelPartBuilder.create().uv(0, 21).cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal3 = bone.addChild("pedal3", ModelPartBuilder.create().uv(0, 20).cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(4.0F, -2.0F, 1.0F, 0.0F, 0.0F, 0.6109F));

		ModelPartData cube_r3 = pedal3.addChild("cube_r3", ModelPartBuilder.create().uv(0, 21).cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal4 = bone.addChild("pedal4", ModelPartBuilder.create().uv(0, 20).cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(5.0F, -1.0F, 1.0F, 0.0F, 0.0F, 1.1781F));

		ModelPartData cube_r4 = pedal4.addChild("cube_r4", ModelPartBuilder.create().uv(0, 21).cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal5 = bone.addChild("pedal5", ModelPartBuilder.create().uv(0, 20).cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(5.0F, 1.0F, 1.0F, 0.0F, 0.0F, 1.5708F));

		ModelPartData cube_r5 = pedal5.addChild("cube_r5", ModelPartBuilder.create().uv(0, 21).cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal6 = bone.addChild("pedal6", ModelPartBuilder.create().uv(0, 20).cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(4.0F, 3.0F, 1.0F, 0.0F, 0.0F, 2.0071F));

		ModelPartData cube_r6 = pedal6.addChild("cube_r6", ModelPartBuilder.create().uv(0, 21).cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal7 = bone.addChild("pedal7", ModelPartBuilder.create().uv(0, 20).cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(3.0F, 4.0F, 1.0F, 0.0F, 0.0F, 2.6616F));

		ModelPartData cube_r7 = pedal7.addChild("cube_r7", ModelPartBuilder.create().uv(0, 21).cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal8 = bone.addChild("pedal8", ModelPartBuilder.create().uv(0, 20).cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(1.0F, 4.0F, 1.0F, 0.0F, 0.0F, 3.0107F));

		ModelPartData cube_r8 = pedal8.addChild("cube_r8", ModelPartBuilder.create().uv(0, 21).cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData bone2 = bone.addChild("bone2", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 12.0F, 0.0F));

		ModelPartData pedal9 = bone2.addChild("pedal9", ModelPartBuilder.create().uv(0, 20).mirrored().cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.pivot(0.0F, -15.0F, 1.0F));

		ModelPartData cube_r9 = pedal9.addChild("cube_r9", ModelPartBuilder.create().uv(0, 21).mirrored().cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal10 = bone2.addChild("pedal10", ModelPartBuilder.create().uv(0, 20).mirrored().cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(-2.0F, -15.0F, 1.0F, 0.0F, 0.0F, -0.3491F));

		ModelPartData cube_r10 = pedal10.addChild("cube_r10", ModelPartBuilder.create().uv(0, 21).mirrored().cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal11 = bone2.addChild("pedal11", ModelPartBuilder.create().uv(0, 20).mirrored().cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(-4.0F, -14.0F, 1.0F, 0.0F, 0.0F, -0.6109F));

		ModelPartData cube_r11 = pedal11.addChild("cube_r11", ModelPartBuilder.create().uv(0, 21).mirrored().cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal12 = bone2.addChild("pedal12", ModelPartBuilder.create().uv(0, 20).mirrored().cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(-5.0F, -13.0F, 1.0F, 0.0F, 0.0F, -1.1781F));

		ModelPartData cube_r12 = pedal12.addChild("cube_r12", ModelPartBuilder.create().uv(0, 21).mirrored().cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal13 = bone2.addChild("pedal13", ModelPartBuilder.create().uv(0, 20).mirrored().cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(-5.0F, -11.0F, 1.0F, 0.0F, 0.0F, -1.5708F));

		ModelPartData cube_r13 = pedal13.addChild("cube_r13", ModelPartBuilder.create().uv(0, 21).mirrored().cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal14 = bone2.addChild("pedal14", ModelPartBuilder.create().uv(0, 20).mirrored().cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(-4.0F, -9.0F, 1.0F, 0.0F, 0.0F, -2.0071F));

		ModelPartData cube_r14 = pedal14.addChild("cube_r14", ModelPartBuilder.create().uv(0, 21).mirrored().cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal15 = bone2.addChild("pedal15", ModelPartBuilder.create().uv(0, 20).mirrored().cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(-3.0F, -8.0F, 1.0F, 0.0F, 0.0F, -2.6616F));

		ModelPartData cube_r15 = pedal15.addChild("cube_r15", ModelPartBuilder.create().uv(0, 21).mirrored().cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData pedal16 = bone2.addChild("pedal16", ModelPartBuilder.create().uv(0, 20).mirrored().cuboid(-1.0F, -4.0F, 0.0F, 2.0F, 3.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(-1.0F, -8.0F, 1.0F, 0.0F, 0.0F, -3.0107F));

		ModelPartData cube_r16 = pedal16.addChild("cube_r16", ModelPartBuilder.create().uv(0, 21).mirrored().cuboid(-1.0F, 0.0F, 0.0F, 2.0F, 2.0F, 0.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.7854F, 0.0F, 0.0F));

		ModelPartData stem = sunflower.addChild("stem", ModelPartBuilder.create().uv(1, 7).cuboid(-0.5F, -8.0F, -0.5F, 1.0F, 8.0F, 1.0F, new Dilation(0.0F)), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData base = sunflower.addChild("base", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData left1 = base.addChild("left1", ModelPartBuilder.create().uv(-4, 0).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		ModelPartData cube_r17 = left1.addChild("cube_r17", ModelPartBuilder.create().uv(0, 2).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r18 = left1.addChild("cube_r18", ModelPartBuilder.create().uv(0, 4).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData left2 = base.addChild("left2", ModelPartBuilder.create().uv(-4, 0).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		ModelPartData cube_r19 = left2.addChild("cube_r19", ModelPartBuilder.create().uv(0, 2).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r20 = left2.addChild("cube_r20", ModelPartBuilder.create().uv(0, 4).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData left3 = base.addChild("left3", ModelPartBuilder.create().uv(-4, 0).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, -2.3562F, 0.0F));

		ModelPartData cube_r21 = left3.addChild("cube_r21", ModelPartBuilder.create().uv(0, 2).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r22 = left3.addChild("cube_r22", ModelPartBuilder.create().uv(0, 4).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData left4 = base.addChild("left4", ModelPartBuilder.create().uv(-4, 0).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 2.3562F, 0.0F));

		ModelPartData cube_r23 = left4.addChild("cube_r23", ModelPartBuilder.create().uv(0, 2).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r24 = left4.addChild("cube_r24", ModelPartBuilder.create().uv(0, 4).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));
		return TexturedModelData.of(modelData, 32, 32);
	}
	@Override
	public void setAngles(SunflowerEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
		this.getPart().traverse().forEach(ModelPart::resetTransform);

		this.updateAnimation(entity.idlingAnimationState, SunflowerAnimations.SUNFLOWER_IDLE,ageInTicks, 1f);
	}
	@Override
	public void render(MatrixStack matrices, VertexConsumer vertexConsumer, int light, int overlay, float red, float green, float blue, float alpha) {
		sunflower.render(matrices, vertexConsumer, light, overlay, red, green, blue, alpha);
	}

	@Override
	public ModelPart getPart() {
		return sunflower;
	}
}