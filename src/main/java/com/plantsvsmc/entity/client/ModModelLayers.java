package com.plantsvsmc.entity.client;

import com.plantsvsmc.PlantsVsMC;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.util.Identifier;

public class ModModelLayers {
    public static final EntityModelLayer SUNFLOWER =
            new EntityModelLayer(new Identifier(PlantsVsMC.MOD_ID, "sunflower"), "main");
    public static final EntityModelLayer SNOWPEA =
            new EntityModelLayer(new Identifier(PlantsVsMC.MOD_ID, "snowpea"), "main");
    public static final EntityModelLayer MELONPULTS =
            new EntityModelLayer(new Identifier(PlantsVsMC.MOD_ID, "melonpults"), "main");
    public static final EntityModelLayer COCONUTCANNON =
            new EntityModelLayer(new Identifier(PlantsVsMC.MOD_ID, "coconut_cannon"), "main");
    public static final EntityModelLayer PZOMBIE =
            new EntityModelLayer(new Identifier(PlantsVsMC.MOD_ID, "pzombie"), "main");
}
