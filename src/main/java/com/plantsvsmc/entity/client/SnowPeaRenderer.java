package com.plantsvsmc.entity.client;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.entity.custom.SnowPeaEntity;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.util.Identifier;

public class SnowPeaRenderer extends MobEntityRenderer<SnowPeaEntity,SnowPeaModel<SnowPeaEntity>> {
    private static final Identifier TEXTURE = new Identifier(PlantsVsMC.MOD_ID,"textures/entity/snowpea.png");
    public SnowPeaRenderer(EntityRendererFactory.Context context) {
        super(context, new SnowPeaModel<>(context.getPart(ModModelLayers.SNOWPEA)), 0.7f);
    }

    @Override
    public Identifier getTexture(SnowPeaEntity entity) {
        return TEXTURE;
    }
}
