package com.plantsvsmc.entity.client;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.entity.custom.PeashooterEnitity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import software.bernie.geckolib.constant.DataTickets;
import software.bernie.geckolib.core.animatable.model.CoreGeoBone;
import software.bernie.geckolib.core.animation.AnimationState;
import software.bernie.geckolib.model.GeoModel;
import software.bernie.geckolib.model.data.EntityModelData;

public class PeashooterModel extends GeoModel<PeashooterEnitity> {
    @Override
    public Identifier getModelResource(PeashooterEnitity animatable) {
        return new Identifier(PlantsVsMC.MOD_ID,"geo/peashooter.geo.json");
    }

    @Override
    public Identifier getTextureResource(PeashooterEnitity animatable) {
        return new Identifier(PlantsVsMC.MOD_ID,"textures/entity/peashooter.png");
    }

    @Override
    public Identifier getAnimationResource(PeashooterEnitity animatable) {
        return new Identifier(PlantsVsMC.MOD_ID,"animations/peashooter.animation.json");
    }


}
