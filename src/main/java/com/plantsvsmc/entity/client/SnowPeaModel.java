package com.plantsvsmc.entity.client;

import com.plantsvsmc.entity.animation.ModAnimations;
import com.plantsvsmc.entity.custom.SnowPeaEntity;
import net.minecraft.client.model.*;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.render.entity.model.SinglePartEntityModel;
import net.minecraft.client.util.math.MatrixStack;

import javax.swing.text.html.parser.Entity;

// Made with Blockbench 4.9.2
// Exported for Minecraft version 1.17+ for Yarn
// Paste this class into your mod and generate all required imports
public class SnowPeaModel<T extends SnowPeaEntity> extends SinglePartEntityModel<T> {
	private final ModelPart snowpea;
	private final ModelPart head;
	private final ModelPart stem;
	private final ModelPart base;

	public SnowPeaModel(ModelPart root) {

		this.snowpea = root.getChild("snowpea");
		this.head = snowpea.getChild("head");
		this.stem = snowpea.getChild("stem");
		this.base = snowpea.getChild("base");
	}
	public static TexturedModelData getTexturedModelData() {
		ModelData modelData = new ModelData();
		ModelPartData modelPartData = modelData.getRoot();
		ModelPartData snowpea = modelPartData.addChild("snowpea", ModelPartBuilder.create(), ModelTransform.of(0.0F, 24.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		ModelPartData head = snowpea.addChild("head", ModelPartBuilder.create().uv(0, 0).cuboid(-3.0F, -15.0F, -3.0F, 6.0F, 8.0F, 6.0F, new Dilation(0.0F))
				.uv(6, 0).cuboid(-2.0F, -13.0F, 4.0F, 4.0F, 4.0F, 2.0F, new Dilation(0.0F))
				.uv(8, 3).cuboid(-2.0F, -14.0F, 6.0F, 4.0F, 2.0F, 1.0F, new Dilation(0.0F))
				.uv(8, 3).cuboid(-2.0F, -10.0F, 6.0F, 4.0F, 2.0F, 1.0F, new Dilation(0.0F))
				.uv(6, 0).cuboid(1.0F, -13.0F, 6.0F, 2.0F, 4.0F, 1.0F, new Dilation(0.0F))
				.uv(6, 1).cuboid(-3.0F, -13.0F, 6.0F, 2.0F, 4.0F, 1.0F, new Dilation(0.0F)), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData cube_r1 = head.addChild("cube_r1", ModelPartBuilder.create().uv(0, 14).cuboid(-3.0F, -4.0F, -14.0F, 6.0F, 8.0F, 6.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, -1.5708F));

		ModelPartData cube_r2 = head.addChild("cube_r2", ModelPartBuilder.create().uv(0, 28).cuboid(-3.0F, -4.0F, -14.0F, 6.0F, 8.0F, 6.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, -1.5708F, 0.0F, 0.0F));

		ModelPartData ice = head.addChild("ice", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -11.0F, -4.0F));

		ModelPartData cube_r3 = ice.addChild("cube_r3", ModelPartBuilder.create().uv(0, 58).cuboid(0.0F, 0.0F, -4.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.151F, -0.1629F, 0.9052F));

		ModelPartData cube_r4 = ice.addChild("cube_r4", ModelPartBuilder.create().uv(0, 58).cuboid(0.0F, 0.0F, -4.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.4259F, -0.2082F, 0.5081F));

		ModelPartData cube_r5 = ice.addChild("cube_r5", ModelPartBuilder.create().uv(0, 58).cuboid(-1.0F, 1.0F, -4.0F, 2.0F, 1.0F, 5.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, 0.2302F, 0.0713F, 1.3294F));

		ModelPartData cube_r6 = ice.addChild("cube_r6", ModelPartBuilder.create().uv(0, 57).cuboid(-1.0F, 1.0F, -5.0F, 2.0F, 2.0F, 5.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, 0.4329F, -0.2353F, -0.4011F));

		ModelPartData cube_r7 = ice.addChild("cube_r7", ModelPartBuilder.create().uv(0, 57).cuboid(-1.0F, 1.0F, -5.0F, 2.0F, 2.0F, 5.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, 0.1952F, -0.1012F, 0.5977F));

		ModelPartData cube_r8 = ice.addChild("cube_r8", ModelPartBuilder.create().uv(0, 57).cuboid(-1.0F, -1.0F, -5.0F, 2.0F, 2.0F, 5.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 0.0F, -0.493F, 0.2929F, 0.493F));

		ModelPartData stem = snowpea.addChild("stem", ModelPartBuilder.create().uv(24, 6).cuboid(-0.5F, -8.0F, -0.5F, 1.0F, 8.0F, 1.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.1309F, 0.0F, 0.0F));

		ModelPartData base = snowpea.addChild("base", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData left1 = base.addChild("left1", ModelPartBuilder.create().uv(14, 0).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		ModelPartData cube_r9 = left1.addChild("cube_r9", ModelPartBuilder.create().uv(22, -2).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r10 = left1.addChild("cube_r10", ModelPartBuilder.create().uv(22, 0).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData left2 = base.addChild("left2", ModelPartBuilder.create().uv(14, 0).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 0.7854F, 0.0F));

		ModelPartData cube_r11 = left2.addChild("cube_r11", ModelPartBuilder.create().uv(22, -2).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r12 = left2.addChild("cube_r12", ModelPartBuilder.create().uv(22, 0).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData left3 = base.addChild("left3", ModelPartBuilder.create().uv(14, 0).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, -2.3562F, 0.0F));

		ModelPartData cube_r13 = left3.addChild("cube_r13", ModelPartBuilder.create().uv(22, -2).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r14 = left3.addChild("cube_r14", ModelPartBuilder.create().uv(22, 0).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData left4 = base.addChild("left4", ModelPartBuilder.create().uv(14, 0).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 2.3562F, 0.0F));

		ModelPartData cube_r15 = left4.addChild("cube_r15", ModelPartBuilder.create().uv(22, -2).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r16 = left4.addChild("cube_r16", ModelPartBuilder.create().uv(22, 0).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));
		return TexturedModelData.of(modelData, 64, 64);
	}
	@Override
	public void setAngles(SnowPeaEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
		this.getPart().traverse().forEach(ModelPart::resetTransform);

		this.updateAnimation(entity.idlingAnimationState, ModAnimations.SNOWPEA_IDLE,ageInTicks, 1f);
	}
	@Override
	public void render(MatrixStack matrices, VertexConsumer vertexConsumer, int light, int overlay, float red, float green, float blue, float alpha) {
		snowpea.render(matrices, vertexConsumer, light, overlay, red, green, blue, alpha);
	}

	@Override
	public ModelPart getPart() {
		return snowpea;
	}
}