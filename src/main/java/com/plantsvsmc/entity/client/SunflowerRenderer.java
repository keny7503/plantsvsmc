package com.plantsvsmc.entity.client;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.entity.custom.SunflowerEntity;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.util.Identifier;

public class SunflowerRenderer extends MobEntityRenderer<SunflowerEntity, SunflowerModel<SunflowerEntity>> {

    private static final Identifier TEXTURE = new Identifier(PlantsVsMC.MOD_ID,"textures/entity/sunflower.png");
    public SunflowerRenderer(EntityRendererFactory.Context context) {
        super(context, new SunflowerModel<>(context.getPart(ModModelLayers.SUNFLOWER)), 0.5f);
    }

    @Override
    public Identifier getTexture(SunflowerEntity entity) {
        return TEXTURE;
    }
}
