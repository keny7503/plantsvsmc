package com.plantsvsmc.entity.client;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.entity.custom.CoconutCannonEntity;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.util.Identifier;

public class CoconutCannonRenderer extends MobEntityRenderer<CoconutCannonEntity,CoconutCannonModel<CoconutCannonEntity>> {
    private static final Identifier TEXTURE = new Identifier(PlantsVsMC.MOD_ID,"textures/entity/coconut_cannon.png");
    public CoconutCannonRenderer(EntityRendererFactory.Context context) {
        super(context, new CoconutCannonModel<>(context.getPart(ModModelLayers.COCONUTCANNON)), 0.5f);
    }

    @Override
    public Identifier getTexture(CoconutCannonEntity entity) {
        return TEXTURE;
    }
}