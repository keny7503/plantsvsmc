package com.plantsvsmc.entity.client;

import com.plantsvsmc.entity.animation.ModAnimations;
import com.plantsvsmc.entity.custom.MelonPultsEntity;
import net.minecraft.client.model.*;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.SinglePartEntityModel;
import net.minecraft.client.util.math.MatrixStack;

public class MelonPultsModel<T extends MelonPultsEntity> extends SinglePartEntityModel<T> {
	private final ModelPart Melon_pults;
	private final ModelPart pasket;
	private final ModelPart tail;
	private final ModelPart body;
	public MelonPultsModel(ModelPart root) {
		this.Melon_pults = root.getChild("Melon_pults");
		this.body = Melon_pults.getChild("body");
		this.tail = Melon_pults.getChild("tail");
		this.pasket = Melon_pults.getChild("pasket");
	}
	public static TexturedModelData getTexturedModelData() {
		ModelData modelData = new ModelData();
		ModelPartData modelPartData = modelData.getRoot();
		ModelPartData Melon_pults = modelPartData.addChild("Melon_pults", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 24.0F, 0.0F));

		ModelPartData pasket = Melon_pults.addChild("pasket", ModelPartBuilder.create().uv(0, 16).cuboid(-4.0F, -10.0F, 5.0F, 8.0F, 1.0F, 8.0F, new Dilation(0.0F))
		.uv(38, 35).cuboid(-3.0F, -9.0F, 6.0F, 6.0F, 1.0F, 6.0F, new Dilation(0.0F))
		.uv(54, 0).cuboid(4.0F, -12.0F, 5.0F, 1.0F, 2.0F, 8.0F, new Dilation(0.0F)), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData pasketwall4_r1 = pasket.addChild("pasketwall4_r1", ModelPartBuilder.create().uv(44, 6).cuboid(4.0F, -2.0F, -4.0F, 1.0F, 2.0F, 8.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -10.0F, 9.0F, 0.0F, 1.5708F, 0.0F));

		ModelPartData pasketwall3_r1 = pasket.addChild("pasketwall3_r1", ModelPartBuilder.create().uv(24, 50).cuboid(4.0F, -2.0F, -4.0F, 1.0F, 2.0F, 8.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -10.0F, 9.0F, 0.0F, 3.1416F, 0.0F));

		ModelPartData pasketwall2_r1 = pasket.addChild("pasketwall2_r1", ModelPartBuilder.create().uv(52, 16).cuboid(4.0F, -2.0F, -4.0F, 1.0F, 2.0F, 8.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -10.0F, 9.0F, 0.0F, -1.5708F, 0.0F));

		ModelPartData melon = pasket.addChild("melon", ModelPartBuilder.create().uv(29, 23).cuboid(-3.6788F, -2.5458F, -3.391F, 7.0F, 5.0F, 7.0F, new Dilation(0.0F))
		.uv(16, 56).cuboid(-4.6788F, -2.2958F, -3.141F, 1.0F, 4.5F, 6.5F, new Dilation(0.0F))
		.uv(56, 29).cuboid(-5.6788F, -2.0458F, -2.891F, 1.0F, 4.0F, 6.0F, new Dilation(0.0F))
		.uv(10, 30).cuboid(-6.6788F, -1.7958F, -2.641F, 1.0F, 3.5F, 5.5F, new Dilation(0.0F)), ModelTransform.of(0.0F, -14.0F, 11.0F, 1.5641F, 1.2657F, 1.5775F));

		ModelPartData bodyright3_r1 = melon.addChild("bodyright3_r1", ModelPartBuilder.create().uv(25, 0).cuboid(-7.0F, -2.25F, -2.75F, 2.0F, 3.5F, 5.5F, new Dilation(0.0F))
		.uv(0, 56).cuboid(-6.0F, -2.5F, -3.0F, 2.0F, 4.0F, 6.0F, new Dilation(0.0F))
		.uv(36, 55).cuboid(-5.0F, -2.75F, -3.25F, 2.0F, 4.25F, 6.5F, new Dilation(0.0F)), ModelTransform.of(-0.6788F, 0.4542F, 0.109F, 0.0F, 3.1416F, 0.0F));

		ModelPartData tail = Melon_pults.addChild("tail", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData tail2_r1 = tail.addChild("tail2_r1", ModelPartBuilder.create().uv(0, 5).cuboid(-1.0F, -1.0F, -1.0F, 2.0F, 3.0F, 1.8F, new Dilation(-0.0001F)), ModelTransform.of(0.0F, -8.2321F, 6.5981F, -0.5236F, 0.0F, 0.0F));

		ModelPartData tail1_r1 = tail.addChild("tail1_r1", ModelPartBuilder.create().uv(0, 0).cuboid(-1.0F, -2.0F, -1.0F, 2.0F, 3.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -6.0F, 4.0F, -1.0472F, 0.0F, 0.0F));

		ModelPartData root = Melon_pults.addChild("root", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData base = root.addChild("base", ModelPartBuilder.create().uv(56, 54).cuboid(-3.75F, -1.0F, -4.0F, 7.5F, 1.0F, 8.0F, new Dilation(0.0F)), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData left1 = root.addChild("left1", ModelPartBuilder.create().uv(8, 25).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(-3.0F, 0.0F, 3.0F, 0.0F, -0.7854F, 0.0F));

		ModelPartData cube_r1 = left1.addChild("cube_r1", ModelPartBuilder.create().uv(0, 20).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r2 = left1.addChild("cube_r2", ModelPartBuilder.create().uv(4, 20).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData left2 = root.addChild("left2", ModelPartBuilder.create().uv(0, 25).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(3.0F, 0.0F, 3.0F, 0.0F, 0.7854F, 0.0F));

		ModelPartData cube_r3 = left2.addChild("cube_r3", ModelPartBuilder.create().uv(0, 18).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r4 = left2.addChild("cube_r4", ModelPartBuilder.create().uv(4, 18).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData left3 = root.addChild("left3", ModelPartBuilder.create().uv(20, 20).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(-3.0F, 0.0F, -3.0F, 0.0F, -2.3562F, 0.0F));

		ModelPartData cube_r5 = left3.addChild("cube_r5", ModelPartBuilder.create().uv(0, 16).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r6 = left3.addChild("cube_r6", ModelPartBuilder.create().uv(4, 16).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData left4 = root.addChild("left4", ModelPartBuilder.create().uv(20, 16).cuboid(-2.0F, -1.0F, 1.0F, 4.0F, 0.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(3.0F, 0.0F, -3.0F, 0.0F, 2.3562F, 0.0F));

		ModelPartData cube_r7 = left4.addChild("cube_r7", ModelPartBuilder.create().uv(0, 14).cuboid(0.0F, -1.0F, 0.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 5.0F, 0.0F, 0.7854F, 1.5708F));

		ModelPartData cube_r8 = left4.addChild("cube_r8", ModelPartBuilder.create().uv(4, 14).cuboid(0.0F, -1.0F, -2.0F, 0.0F, 2.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -1.0F, 1.0F, 0.0F, -0.7854F, 1.5708F));

		ModelPartData body = Melon_pults.addChild("body", ModelPartBuilder.create().uv(0, 0).cuboid(-4.0F, -8.0F, -4.5F, 8.0F, 7.0F, 9.0F, new Dilation(0.0F))
		.uv(28, 36).cuboid(-5.0F, -7.75F, -4.25F, 1.0F, 6.5F, 8.5F, new Dilation(0.0F))
		.uv(18, 30).cuboid(-6.0F, -7.5F, -4.0F, 1.0F, 6.0F, 8.0F, new Dilation(0.0F))
		.uv(48, 48).cuboid(-7.0F, -7.25F, -3.75F, 1.0F, 5.5F, 7.5F, new Dilation(0.0F))
		.uv(16, 44).cuboid(-8.0F, -7.0F, -3.5F, 1.0F, 5.0F, 7.0F, new Dilation(0.0F)), ModelTransform.pivot(0.0F, 0.5F, 0.0F));

		ModelPartData bodyright4_r1 = body.addChild("bodyright4_r1", ModelPartBuilder.create().uv(0, 44).cuboid(-8.0F, -3.0F, -3.5F, 1.0F, 5.0F, 7.0F, new Dilation(0.0F))
		.uv(39, 43).cuboid(-7.0F, -3.25F, -3.75F, 1.0F, 5.5F, 7.5F, new Dilation(0.0F))
		.uv(0, 30).cuboid(-6.0F, -3.5F, -4.0F, 1.0F, 6.0F, 8.0F, new Dilation(0.0F))
		.uv(34, 0).cuboid(-5.0F, -3.75F, -4.25F, 1.0F, 6.25F, 8.5F, new Dilation(0.0F)), ModelTransform.of(0.0F, -4.0F, 0.0F, 0.0F, 3.1416F, 0.0F));
		return TexturedModelData.of(modelData, 128, 128);
	}

	@Override
	public void setAngles(MelonPultsEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
		this.getPart().traverse().forEach(ModelPart::resetTransform);

		this.updateAnimation(entity.idlingAnimationState, ModAnimations.MELONPULTS_IDLE,ageInTicks, 1f);
		this.updateAnimation(entity.attackAnimationState, ModAnimations.MELONPULTS_ATTACK,ageInTicks, 1f);
	}

	@Override
	public void render(MatrixStack matrices, VertexConsumer vertexConsumer, int light, int overlay, float red, float green, float blue, float alpha) {
		Melon_pults.render(matrices, vertexConsumer, light, overlay, red, green, blue, alpha);
	}
	@Override
	public ModelPart getPart() {
		return Melon_pults;
	}
}