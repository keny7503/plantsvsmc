package com.plantsvsmc.entity.client;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.entity.custom.PeashooterEnitity;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import software.bernie.geckolib.model.GeoModel;
import software.bernie.geckolib.renderer.GeoEntityRenderer;

public class PeashooterRenderer extends GeoEntityRenderer<PeashooterEnitity> {
    public PeashooterRenderer(EntityRendererFactory.Context renderManager) {
        super(renderManager, new PeashooterModel());
    }

    @Override
    public Identifier getTextureLocation(PeashooterEnitity animatable) {
        return new Identifier(PlantsVsMC.MOD_ID,"textures/entity/peashooter.png");
    }

    @Override
    public void render(PeashooterEnitity entity, float entityYaw, float partialTick, MatrixStack poseStack, VertexConsumerProvider bufferSource, int packedLight) {
        super.render(entity, entityYaw, partialTick, poseStack, bufferSource, packedLight);
    }
}
