package com.plantsvsmc.entity.client;// Made with Blockbench 4.9.2
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.plantsvsmc.entity.animation.CoconutCannonAnimations;
import com.plantsvsmc.entity.animation.SunflowerAnimations;
import com.plantsvsmc.entity.custom.CoconutCannonEntity;
import com.plantsvsmc.entity.custom.SnowPeaEntity;
import net.minecraft.client.model.*;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.SinglePartEntityModel;
import net.minecraft.client.util.math.MatrixStack;

public class CoconutCannonModel<T extends CoconutCannonEntity> extends SinglePartEntityModel<T>{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	private final ModelPart coconut_canon;
	private final ModelPart body;
	private final ModelPart wheel;
	private final ModelPart smoke;
	private final ModelPart eyebrown;
	private final ModelPart wheel1;
	private final ModelPart wheel2;
	private final ModelPart fire;





	public CoconutCannonModel(ModelPart root) {
        coconut_canon = root.getChild("coconut_canon");
        this.body = coconut_canon.getChild("body");
		this.eyebrown = body.getChild("eyebrown");
        this.wheel = coconut_canon.getChild("wheel");
		this.wheel1 = wheel.getChild("wheel1");
		this.wheel2 = wheel.getChild("wheel2");
		this.fire = body.getChild("fire");
        this.smoke = coconut_canon.getChild("smoke");
    }

	public static TexturedModelData getTexturedModelData() {
		ModelData modelData = new ModelData();
		ModelPartData modelPartData = modelData.getRoot();
		ModelPartData coconut_canon = modelPartData.addChild("coconut_canon", ModelPartBuilder.create(), ModelTransform.of(0.0F, 24.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		ModelPartData body = coconut_canon.addChild("body", ModelPartBuilder.create().uv(0, 0).cuboid(-3.0F, -3.0F, -5.0F, 6.0F, 6.0F, 12.0F, new Dilation(0.0F))
				.uv(48, 0).cuboid(-2.0F, 2.0F, 7.0F, 4.0F, 1.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -5.0F, -3.0F, 0.1745F, 0.0F, 0.0F));

		ModelPartData cube_r1 = body.addChild("cube_r1", ModelPartBuilder.create().uv(0, 18).cuboid(-4.0F, -4.0F, -4.0F, 8.0F, 10.0F, 8.0F, new Dilation(0.0F))
				.uv(22, 26).cuboid(-3.0F, -3.0F, -5.0F, 6.0F, 6.0F, 10.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 1.5708F, 0.0F, 0.0F));

		ModelPartData cube_r2 = body.addChild("cube_r2", ModelPartBuilder.create().uv(25, 9).cuboid(-0.5F, 0.1481F, 0.2284F, 1.0F, 1.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -8.0F, -4.0F, 2.1817F, 0.0F, 0.0F));

		ModelPartData cube_r3 = body.addChild("cube_r3", ModelPartBuilder.create().uv(24, 8).cuboid(-0.5F, -1.0F, 0.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -8.0F, -1.0F, 2.7489F, 0.0F, 0.0F));

		ModelPartData cube_r4 = body.addChild("cube_r4", ModelPartBuilder.create().uv(24, 8).cuboid(-0.5F, 0.0F, -3.0F, 1.0F, 1.0F, 3.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -8.0F, -2.0F, 1.5708F, 0.0F, 0.0F));

		ModelPartData cube_r5 = body.addChild("cube_r5", ModelPartBuilder.create().uv(26, 8).cuboid(-3.0F, -3.0F, -5.0F, 6.0F, 6.0F, 10.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 1.5708F, 0.0F));

		ModelPartData cube_r6 = body.addChild("cube_r6", ModelPartBuilder.create().uv(48, 0).cuboid(-2.0F, 2.0F, 7.0F, 4.0F, 1.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		ModelPartData cube_r7 = body.addChild("cube_r7", ModelPartBuilder.create().uv(48, 0).cuboid(-2.0F, 2.0F, 7.0F, 4.0F, 1.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.5708F));

		ModelPartData cube_r8 = body.addChild("cube_r8", ModelPartBuilder.create().uv(48, 0).cuboid(-2.0F, 2.0F, 7.0F, 4.0F, 1.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -3.1416F));

		ModelPartData eyebrown = body.addChild("eyebrown", ModelPartBuilder.create().uv(0, -4).cuboid(4.1F, -4.0F, 2.0F, 0.0F, 2.0F, 4.0F, new Dilation(0.0F))
				.uv(0, -4).mirrored().cuboid(-4.1F, -4.0F, 2.0F, 0.0F, 2.0F, 4.0F, new Dilation(0.0F)).mirrored(false), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData fire = body.addChild("fire", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -10.0F, -5.0F));

		ModelPartData cube_r9 = fire.addChild("cube_r9", ModelPartBuilder.create().uv(24, 3).cuboid(-1.0F, -0.9736F, 0.4092F, 2.0F, 2.0F, 3.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 1.0F, 1.0F, 2.3126F, 0.0F, 0.0F));

		ModelPartData wheel = coconut_canon.addChild("wheel", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData wheel1 = wheel.addChild("wheel1", ModelPartBuilder.create().uv(0, 6).cuboid(5.0F, -2.9F, -4.0F, 3.0F, 3.0F, 3.0F, new Dilation(0.0F))
				.uv(-3, 23).cuboid(5.0F, -3.0F, -4.0F, 3.0F, 0.0F, 3.0F, new Dilation(0.0F)), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData cube_r10 = wheel1.addChild("cube_r10", ModelPartBuilder.create().uv(1, 24).cuboid(-1.0F, 0.0F, -2.0F, 3.0F, 0.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(6.0F, -3.0F, -4.0F, 1.3963F, 0.0F, 0.0F));

		ModelPartData bone = wheel1.addChild("bone", ModelPartBuilder.create(), ModelTransform.of(7.0F, -3.0F, 3.0F, 0.1745F, 0.0F, 0.0F));

		ModelPartData cube_r11 = bone.addChild("cube_r11", ModelPartBuilder.create().uv(-2, 24).cuboid(-1.0F, -3.9392F, -1.3054F, 3.0F, 0.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(-1.0F, 0.0F, 0.0F, 1.5708F, 0.0F, 0.0F));

		ModelPartData cube_r12 = bone.addChild("cube_r12", ModelPartBuilder.create().uv(-2, 19).cuboid(-1.0F, -3.3736F, 0.1492F, 3.0F, 0.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(-1.0F, 2.0F, 0.0F, 1.9635F, 0.0F, 0.0F));

		ModelPartData wheel2 = wheel.addChild("wheel2", ModelPartBuilder.create().uv(0, 6).cuboid(5.0F, -2.9F, -4.0F, 3.0F, 3.0F, 3.0F, new Dilation(0.0F))
				.uv(-3, 23).cuboid(5.0F, -3.0F, -4.0F, 3.0F, 0.0F, 3.0F, new Dilation(0.0F)), ModelTransform.pivot(-13.0F, 0.0F, 0.0F));

		ModelPartData cube_r13 = wheel2.addChild("cube_r13", ModelPartBuilder.create().uv(1, 24).cuboid(-1.0F, 0.0F, -2.0F, 3.0F, 0.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(6.0F, -3.0F, -4.0F, 1.3963F, 0.0F, 0.0F));

		ModelPartData bone2 = wheel2.addChild("bone2", ModelPartBuilder.create(), ModelTransform.of(7.0F, -3.0F, 3.0F, 0.1745F, 0.0F, 0.0F));

		ModelPartData cube_r14 = bone2.addChild("cube_r14", ModelPartBuilder.create().uv(-2, 24).cuboid(-1.0F, -3.9392F, -1.3054F, 3.0F, 0.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(-1.0F, 0.0F, 0.0F, 1.5708F, 0.0F, 0.0F));

		ModelPartData cube_r15 = bone2.addChild("cube_r15", ModelPartBuilder.create().uv(-2, 19).cuboid(-1.0F, -3.3736F, 0.1492F, 3.0F, 0.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(-1.0F, 2.0F, 0.0F, 1.9635F, 0.0F, 0.0F));

		ModelPartData smoke = coconut_canon.addChild("smoke", ModelPartBuilder.create().uv(36, 0).cuboid(4.0F, -4.0F, 12.0F, 1.0F, 8.0F, 0.0F, new Dilation(0.0F)), ModelTransform.pivot(0.0F, -5.0F, -3.0F));

		ModelPartData cube_r16 = smoke.addChild("cube_r16", ModelPartBuilder.create().uv(36, 0).cuboid(4.0F, -4.0F, 12.0F, 1.0F, 8.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -3.1416F));

		ModelPartData cube_r17 = smoke.addChild("cube_r17", ModelPartBuilder.create().uv(36, 0).cuboid(4.0F, -4.0F, 12.0F, 1.0F, 8.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.5708F));

		ModelPartData cube_r18 = smoke.addChild("cube_r18", ModelPartBuilder.create().uv(36, 0).cuboid(4.0F, -4.0F, 12.0F, 1.0F, 8.0F, 0.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));
		return TexturedModelData.of(modelData, 64, 64);
	}
	@Override
	public void setAngles(CoconutCannonEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
		this.getPart().traverse().forEach(ModelPart::resetTransform);
		this.updateAnimation(entity.idleAnimationState, CoconutCannonAnimations.IDLE ,ageInTicks, 1f);
		this.updateAnimation(entity.attackAnimationState, CoconutCannonAnimations.ATTACK ,ageInTicks, 1f);
	}
	@Override
	public void render(MatrixStack matrices, VertexConsumer vertexConsumer, int light, int overlay, float red, float green, float blue, float alpha) {
		coconut_canon.render(matrices, vertexConsumer, light, overlay, red, green, blue, alpha);
	}

	@Override
	public ModelPart getPart() {
		return coconut_canon;
	}
}