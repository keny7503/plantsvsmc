package com.plantsvsmc.entity.client;

import com.plantsvsmc.entity.animation.ModAnimations;
import com.plantsvsmc.entity.custom.PZombieEntiy;

import net.minecraft.client.model.Dilation;
import net.minecraft.client.model.ModelData;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.model.ModelPartBuilder;
import net.minecraft.client.model.ModelPartData;
import net.minecraft.client.model.ModelTransform;
import net.minecraft.client.model.TexturedModelData;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.model.SinglePartEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.MathHelper;

public class PZombieModel <T extends PZombieEntiy> extends SinglePartEntityModel<T>{
	private final ModelPart zombie;
	private final ModelPart head;

	public PZombieModel(ModelPart root) {
		this.zombie = root.getChild("zombie");
		this.head=zombie.getChild("head");
	}
	public static TexturedModelData getTexturedModelData() {
		ModelData modelData = new ModelData();
		ModelPartData modelPartData = modelData.getRoot();
		ModelPartData zombie = modelPartData.addChild("zombie", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 24.0F, 0.0F));

		ModelPartData head = zombie.addChild("head", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, -23.0F, -5.0F));

		ModelPartData head_r1 = head.addChild("head_r1", ModelPartBuilder.create().uv(0, 0).cuboid(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -0.0616F, 0.3516F, 0.2618F, 0.0F, 0.0F));

		ModelPartData body = zombie.addChild("body", ModelPartBuilder.create().uv(0, 16).cuboid(-4.75F, -6.8457F, -2.697F, 9.5F, 11.0F, 5.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, -17.0F, -1.0F, 0.5236F, 0.0F, 0.0F));

		ModelPartData arm1 = body.addChild("arm1", ModelPartBuilder.create().uv(37, 37).mirrored().cuboid(-1.75F, -2.6961F, -2.5261F, 3.5F, 6.0F, 4.5F, new Dilation(0.0F)).mirrored(false)
		.uv(47, 19).cuboid(-1.75F, 3.3039F, -2.5261F, 3.5F, 6.0F, 4.5F, new Dilation(0.0F)), ModelTransform.of(-6.5F, -4.1496F, -0.1708F, -0.6109F, 0.0F, 0.0F));

		ModelPartData arm2 = body.addChild("arm2", ModelPartBuilder.create().uv(0, 32).cuboid(-1.75F, -2.6961F, -2.5261F, 3.5F, 6.0F, 4.5F, new Dilation(0.0F))
		.uv(13, 38).mirrored().cuboid(-1.75F, 3.3039F, -2.5261F, 3.5F, 6.0F, 4.5F, new Dilation(0.0F)).mirrored(false), ModelTransform.of(6.5F, -4.1496F, -0.1708F, -0.5236F, 0.0F, 0.0F));

		ModelPartData lower_body = zombie.addChild("lower_body", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData leg1 = lower_body.addChild("leg1", ModelPartBuilder.create(), ModelTransform.of(-3.0F, -7.0F, -1.0F, 0.0F, -0.9163F, 0.0F));

		ModelPartData leg1_feat = leg1.addChild("leg1_feat", ModelPartBuilder.create().uv(33, 10).cuboid(-6.0F, -2.0F, -2.75F, 4.0F, 2.0F, 6.25F, new Dilation(0.0F)), ModelTransform.pivot(4.0F, 7.0F, 0.0F));

		ModelPartData leg1_calf1 = leg1.addChild("leg1_calf1", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 3.0F, 0.0F));

		ModelPartData calf1_r1 = leg1_calf1.addChild("calf1_r1", ModelPartBuilder.create().uv(0, 45).cuboid(-1.75F, -5.0F, -2.0F, 3.5F, 6.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 2.0F, 1.0F, 0.2618F, 0.0F, 0.0F));

		ModelPartData leg1_calf2 = leg1.addChild("leg1_calf2", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData calf2_r1 = leg1_calf2.addChild("calf2_r1", ModelPartBuilder.create().uv(26, 28).cuboid(-1.2925F, -6.416F, -3.6046F, 3.5F, 8.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 1.0F, -0.3491F, 0.3927F, 0.0F));

		ModelPartData leg2 = lower_body.addChild("leg2", ModelPartBuilder.create(), ModelTransform.of(3.0F, -7.0F, -1.0F, 0.0F, 0.0873F, 0.0F));

		ModelPartData leg2_feat = leg2.addChild("leg2_feat", ModelPartBuilder.create().uv(32, 0).cuboid(2.0F, -2.0F, -2.75F, 4.0F, 2.0F, 6.25F, new Dilation(0.0F)), ModelTransform.pivot(-4.0F, 7.0F, 0.0F));

		ModelPartData leg2_calf1 = leg2.addChild("leg2_calf1", ModelPartBuilder.create(), ModelTransform.pivot(-0.0436F, 3.0F, 0.4981F));

		ModelPartData leg1_r1 = leg2_calf1.addChild("leg1_r1", ModelPartBuilder.create().uv(49, 5).cuboid(-1.75F, -5.0F, -2.0F, 3.5F, 6.0F, 4.0F, new Dilation(0.0F)), ModelTransform.of(0.0436F, 2.0F, 0.5019F, 0.2182F, 0.0F, 0.0F));

		ModelPartData leg2_calf2 = leg2.addChild("leg2_calf2", ModelPartBuilder.create(), ModelTransform.pivot(0.0F, 0.0F, 0.0F));

		ModelPartData bone_r1 = leg2_calf2.addChild("bone_r1", ModelPartBuilder.create().uv(0, 0).cuboid(-1.0F, -3.0F, -1.0F, 2.0F, 4.0F, 2.0F, new Dilation(0.0F)), ModelTransform.of(0.0F, 0.0F, 0.0F, -0.3054F, 0.0F, 0.0F));

		ModelPartData leg2_r1 = leg2_calf2.addChild("leg2_r1", ModelPartBuilder.create().uv(27, 48).mirrored().cuboid(-1.75F, -3.0F, -2.0F, 3.5F, 5.0F, 4.0F, new Dilation(0.0001F)).mirrored(false), ModelTransform.of(0.0F, -4.0F, 1.0F, -0.3054F, 0.0F, 0.0F));
		return TexturedModelData.of(modelData, 64, 64);
	}
	@Override
	public void setAngles(PZombieEntiy entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        this.getPart().traverse().forEach(ModelPart::resetTransform);
		this.setHeadAngles(netHeadYaw, headPitch);
		this.animateMovement(ModAnimations.PZOMBIE_WALKING, limbSwing, limbSwingAmount, 2f, 2.5f);
		this.updateAnimation(entity.idlingAnimationState, ModAnimations.PZOMBIE_IDLE,ageInTicks, 1f);
	}

	private void setHeadAngles(float headYaw, float headPitch){
		headYaw=MathHelper.clamp(headYaw, -30.0F, 30.0F);
		headPitch=MathHelper.clamp(headPitch, -30.0F, 30.0F);

		this.head.yaw=headYaw*0.017453292f;
		this.head.pitch=headPitch*0.017453292f;
	}

	@Override
	public void render(MatrixStack matrices, VertexConsumer vertexConsumer, int light, int overlay, float red, float green, float blue, float alpha) {
		zombie.render(matrices, vertexConsumer, light, overlay, red, green, blue, alpha);
	}
    @Override
    public ModelPart getPart() {
        return zombie;
    }
}
