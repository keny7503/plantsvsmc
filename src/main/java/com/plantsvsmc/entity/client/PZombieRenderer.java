package com.plantsvsmc.entity.client;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.entity.custom.PZombieEntiy;

import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;

public class PZombieRenderer extends MobEntityRenderer<PZombieEntiy, PZombieModel<PZombieEntiy>>{
    private static final Identifier TEXTURE = new Identifier(PlantsVsMC.MOD_ID,"textures/entity/pzombie.png");
    public PZombieRenderer(EntityRendererFactory.Context context) {
        super(context, new PZombieModel<>(context.getPart(ModModelLayers.PZOMBIE)),0.6f);

    }

    @Override
    public Identifier getTexture(PZombieEntiy entity) {
        return TEXTURE;
    }

    @Override
    public void render(PZombieEntiy mobEntity, float f, float g, MatrixStack matrixStack,
            VertexConsumerProvider vertexConsumerProvider, int i) {

        super.render(mobEntity, f, g, matrixStack, vertexConsumerProvider, i);
    }
}
