package com.plantsvsmc.entity;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.entity.custom.*;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import com.plantsvsmc.entity.client.*;
import net.minecraft.util.Identifier;

public class ModEntities {

    // Entity create:
    // Create Enitity class, Renderer class
    // Import Model, Import library
    // Indentifiy Entity class
    // Register in PlantsVSMC ,
    // Register  ModelLayer in PlantsVSMCClient


    public static final EntityType<SunflowerEntity> SUNFLOWER = Registry.register(Registries.ENTITY_TYPE,
            new Identifier(PlantsVsMC.MOD_ID, "sunflower"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, SunflowerEntity::new)
                    .dimensions(EntityDimensions.fixed(1f, 1f)).build());
    public static final EntityType<SnowPeaEntity> SNOWPEA = Registry.register(Registries.ENTITY_TYPE,
            new Identifier(PlantsVsMC.MOD_ID, "snowpea"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, SnowPeaEntity::new)
                    .dimensions(EntityDimensions.fixed(1f, 1f)).build());
    public static final EntityType<MelonPultsEntity> MELONPULTS = Registry.register(Registries.ENTITY_TYPE,
            new Identifier(PlantsVsMC.MOD_ID, "melonpults"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE , MelonPultsEntity::new)
                    .dimensions(EntityDimensions.fixed(1f, 1f)).build());
    public static final EntityType<CoconutCannonEntity> COCONUTCANNON = Registry.register(Registries.ENTITY_TYPE,
            new Identifier(PlantsVsMC.MOD_ID, "coconut_cannon"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE , CoconutCannonEntity::new)
                    .dimensions(EntityDimensions.fixed(1f, 1f)).build());
    public static final EntityType<PZombieEntiy> PZOMBIE = Registry.register(Registries.ENTITY_TYPE,
            new Identifier(PlantsVsMC.MOD_ID, "pzombie"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE , PZombieEntiy::new)
                    .dimensions(EntityDimensions.fixed(1f, 1f)).build());
    public static final EntityType<PeashooterEnitity> PEASHOOTER = Registry.register(Registries.ENTITY_TYPE,
            new Identifier(PlantsVsMC.MOD_ID, "peashooter"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE , PeashooterEnitity::new)
                    .dimensions(EntityDimensions.fixed(1f, 1f)).build());

    public static final void registerModEnitity(){
        FabricDefaultAttributeRegistry.register(ModEntities.SUNFLOWER, SunflowerEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(ModEntities.SNOWPEA, SnowPeaEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(ModEntities.MELONPULTS, MelonPultsEntity.createMelonPultsAttributes());
        FabricDefaultAttributeRegistry.register(ModEntities.COCONUTCANNON, CoconutCannonEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(ModEntities.PZOMBIE, PZombieEntiy.createPZombieAttribute());
        FabricDefaultAttributeRegistry.register(ModEntities.PEASHOOTER, PeashooterEnitity.setAttributes());


    }
    public static final void registerModModelEnitity(){
        EntityRendererRegistry.register(ModEntities.SUNFLOWER, SunflowerRenderer::new);
        EntityModelLayerRegistry.registerModelLayer(ModModelLayers.SUNFLOWER, SunflowerModel::getTexturedModelData);

        EntityRendererRegistry.register(ModEntities.SNOWPEA, SnowPeaRenderer::new);
        EntityModelLayerRegistry.registerModelLayer(ModModelLayers.SNOWPEA, SnowPeaModel::getTexturedModelData);

        EntityRendererRegistry.register(ModEntities.MELONPULTS, MelonPultsRenderer::new);
        EntityModelLayerRegistry.registerModelLayer(ModModelLayers.MELONPULTS, MelonPultsModel::getTexturedModelData);

        EntityRendererRegistry.register(ModEntities.COCONUTCANNON, CoconutCannonRenderer::new);
        EntityModelLayerRegistry.registerModelLayer(ModModelLayers.COCONUTCANNON, CoconutCannonModel::getTexturedModelData);

        EntityRendererRegistry.register(ModEntities.PZOMBIE, PZombieRenderer::new);
        EntityModelLayerRegistry.registerModelLayer(ModModelLayers.PZOMBIE, PZombieModel::getTexturedModelData);

        EntityRendererRegistry.register(ModEntities.PEASHOOTER, PeashooterRenderer::new);

    }
}
