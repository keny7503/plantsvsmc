package com.plantsvsmc.item.client;

import com.plantsvsmc.item.custom.IronBucketItem;
import software.bernie.geckolib.renderer.GeoArmorRenderer;

public class IronBucketRenderer extends GeoArmorRenderer<IronBucketItem> {
    public IronBucketRenderer() {
        super(new IronBucketModel());
    }
}
