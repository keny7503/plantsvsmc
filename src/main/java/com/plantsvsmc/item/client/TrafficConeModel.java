package com.plantsvsmc.item.client;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.item.custom.TrafficConeItem;
import net.minecraft.util.Identifier;
import software.bernie.geckolib.model.GeoModel;

public class TrafficConeModel extends GeoModel<TrafficConeItem> {
    @Override
    public Identifier getModelResource(TrafficConeItem animatable) {
        return new Identifier(PlantsVsMC.MOD_ID, "geo/traffic_cone.geo.json");
    }

    @Override
    public Identifier getTextureResource(TrafficConeItem animatable) {
        return new Identifier(PlantsVsMC.MOD_ID, "textures/armor/traffic_cone.png");
    }

    @Override
    public Identifier getAnimationResource(TrafficConeItem animatable) {
        return new Identifier(PlantsVsMC.MOD_ID, "animations/traffic_cone.animation.json");
    }
}
