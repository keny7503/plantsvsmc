package com.plantsvsmc.item.client;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.item.custom.IronBucketItem;
import net.minecraft.util.Identifier;
import software.bernie.geckolib.model.GeoModel;

public class IronBucketModel extends GeoModel<IronBucketItem> {
    @Override
    public Identifier getModelResource(IronBucketItem animatable) {
        return new Identifier(PlantsVsMC.MOD_ID, "geo/iron_bucket.geo.json");
    }

    @Override
    public Identifier getTextureResource(IronBucketItem animatable) {
        return new Identifier(PlantsVsMC.MOD_ID, "textures/armor/iron_bucket.png");
    }

    @Override
    public Identifier getAnimationResource(IronBucketItem animatable) {
        return new Identifier(PlantsVsMC.MOD_ID, "animations/iron_bucket.animation.json");
    }
}
