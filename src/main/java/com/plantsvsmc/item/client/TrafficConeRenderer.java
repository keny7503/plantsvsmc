package com.plantsvsmc.item.client;

import com.plantsvsmc.item.custom.TrafficConeItem;

import software.bernie.geckolib.renderer.GeoArmorRenderer;

public class TrafficConeRenderer extends GeoArmorRenderer<TrafficConeItem> {
    public TrafficConeRenderer() {
        super(new TrafficConeModel());
    }
}
