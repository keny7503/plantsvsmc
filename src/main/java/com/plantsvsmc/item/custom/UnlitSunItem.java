package com.plantsvsmc.item.custom;

import com.plantsvsmc.item.ModItems;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class UnlitSunItem extends Item {
    public UnlitSunItem(Settings settings) {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {

        if(user.experienceLevel<1){ return TypedActionResult.fail(user.getStackInHand(hand));}

        user.experienceLevel-=1;
        user.giveItemStack(new ItemStack(ModItems.SUN,1));
        user.getInventory().removeStack(user.getInventory().selectedSlot,1);
        user.playSound(SoundEvents.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.0F);

        return TypedActionResult.consume(user.getStackInHand(hand));
    }


}
