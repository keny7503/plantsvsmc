package com.plantsvsmc.item;

import com.plantsvsmc.PlantsVsMC;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import static com.plantsvsmc.item.ModItems.*;

public class ModItemGroups {
    public static final ItemGroup PVZ_GROUP = FabricItemGroup.builder().displayName(Text.translatable("itemgroup.pvz"))
                    .icon(() -> new ItemStack(SUN)).entries((displayContext, entries) -> {
                        entries.add(PLANTESSENCE);
                        entries.add(FILLEDPLANTESSENCE);
                        entries.add(SUN);
                        entries.add(UNLIT_SUN);
                        entries.add(SUNFLOWER_SPAWN_EGG);
                        entries.add(PEASHOOTER_SPAWN_EGG);
                        entries.add(PEA);
                        entries.add(TRAFFIC_CONE);
                        entries.add(IRON_BUCKET);

                    }).build();


    public static void registerItemGroups() {
        PlantsVsMC.LOGGER.info("Registering Item Groups for " + PlantsVsMC.MOD_ID);
        Registry.register(Registries.ITEM_GROUP, new Identifier(PlantsVsMC.MOD_ID, "pvz"),PVZ_GROUP);
    }
}