package com.plantsvsmc.item;

import com.plantsvsmc.PlantsVsMC;
import com.plantsvsmc.block.ModBlocks;
import com.plantsvsmc.entity.ModEntities;
import com.plantsvsmc.item.custom.IronBucketItem;
import com.plantsvsmc.item.custom.SeedPacketItem;
import com.plantsvsmc.item.custom.TrafficConeItem;
import com.plantsvsmc.item.custom.UnlitSunItem;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroupEntries;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.item.*;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;



public class ModItems {

    public static final Item PLANTESSENCE = registerItem("plantessence", new Item(new FabricItemSettings()));
    public static final Item FILLEDPLANTESSENCE = registerItem("filledplantessence", new Item(new FabricItemSettings()));
    public static final Item SUN = registerItem("sun", new Item(new FabricItemSettings()));
    public static final Item UNLIT_SUN = registerItem("unlit_sun", new UnlitSunItem(new FabricItemSettings()));
    public static final Item SUNFLOWER_SPAWN_EGG = registerItem("sunflower_spawn_egg", new SeedPacketItem(ModEntities.SUNFLOWER, new FabricItemSettings()));
    public static final Item PEASHOOTER_SPAWN_EGG = registerItem("peashooter_spawn_egg", new SeedPacketItem(ModEntities.PEASHOOTER, new FabricItemSettings()));
    public static final Item PEA = registerItem("pea", new AliasedBlockItem(ModBlocks.PEA_CROP, new FabricItemSettings()));
    public static final Item TRAFFIC_CONE = registerItem("traffic_cone", new TrafficConeItem(ModArmorMaterials.TRAFFIC_CONE, ArmorItem.Type.HELMET, new FabricItemSettings()));
    public static final Item IRON_BUCKET = registerItem("iron_bucket", new IronBucketItem(ModArmorMaterials.IRON_BUCKET, ArmorItem.Type.HELMET, new FabricItemSettings()));
    public static void addItemsToIngredientItemGroup(FabricItemGroupEntries entries){
        entries.add(PLANTESSENCE);
        entries.add(FILLEDPLANTESSENCE);
        entries.add(SUN);
        entries.add(UNLIT_SUN);
        entries.add(SUNFLOWER_SPAWN_EGG);
        entries.add(PEA);
        entries.add(TRAFFIC_CONE);
        entries.add(IRON_BUCKET);

    }

    public static Item registerItem(String name, Item item){
        return Registry.register(Registries.ITEM, new Identifier(PlantsVsMC.MOD_ID,name),item);
    }
    public static void registerModItems(){
        PlantsVsMC.LOGGER.info("Registering items for"+ PlantsVsMC.MOD_ID);

//        ItemGroupEvents.modifyEntriesEvent(ItemGroups.INGREDIENTS).register(ModItems::addItemsToIngredientItemGroup);
    }
}
